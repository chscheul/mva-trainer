import pandas as pd
import json
import numpy as np
import tensorflow as tf
import HelperModules.CustomLosses as cl
import HelperModules.HelperFunctions as hf
from numpy.random import random
from HelperModules.MessageHandler import ErrorMessage, WarningMessage, TrainerMessage, TrainerDoneMessage
from HelperModules.OptionChecker import OptionChecker
from HelperModules.BasicModel import BasicModel
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Add, Dense, Dropout, concatenate, BatchNormalization, Input, LeakyReLU, multiply
from tensorflow.keras.initializers import RandomNormal
from tensorflow.keras.callbacks import EarlyStopping, TensorBoard
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras import regularizers
from sklearn.model_selection import train_test_split
        
class DNNModel(BasicModel):
    """
    Class for implementing feed-forward-style neural networks.
    This class serves as a base class for any such type of neural network.
    """
    def __init__(self, group, input_dim, Variables, is_Train=True):
        super().__init__(group)
        self.__InputDim              = input_dim       # Input dimensionality of the data
        self.__kerasmodel            = None            # Keras model object
        self.__PredesignedModel      = None            # For usage of pre-designed models
        self.__Nodes                 = None            # Number of nodes (list) per layer
        self.__LearningRate          = 0.001           # Learning rate
        self.__Patience              = None            # Number of epochs in ES to wait for improvement
        self.__MinDelta              = None            # Minimum improvement for ES
        self.__DropoutIndece         = []              # Layers after which Dropout layers are added
        self.__DropoutProb           = 0.1             # Dropout probability (0.2=20%)
        self.__BatchNormIndece       = []              # Layers after which BatchNormalisation layers are added
        self.__OutputSize            = None            # Size of the output layer
        self.__ActivationFunctions   = None            # List of activations functions
        self.__KernelInitialiser     = "glorot_normal" # Kernel initialisation
        self.__KernelRegulariser     = None            # Regulariser to apply a penalty on the layer's kernel
        self.__BiasRegulariser       = None            # Regulariser to apply a penalty on the layer's biases
        self.__ActivityRegulariser   = None            # Regulariser to apply a penalty on the layer's outputs
        self.__KernelRegulariserl1   = 0.01            # L1 regularisation applied on the layer's kernel
        self.__KernelRegulariserl2   = 0.01            # L2 regularisation applied on the layer's kernel
        self.__BiasRegulariserl1     = 0.01            # L1 regularisation applied on the layer's bias
        self.__BiasRegulariserl2     = 0.01            # L2 regularisation applied on the layer's bias
        self.__ActivityRegulariserl1 = 0.01            # L1 regularisation applied on the layer's outputs
        self.__ActivityRegulariserl2 = 0.01            # L2 regularisation applied on the layer's outputs
        self.__OutputActivation      = None            # Activation function for the output layer
        self.__Loss                  = None            # Loss function to be used
        self.__SmoothingAlpha        = None            # Parameter to be used for label smoothing
        self.__UseTensorBoard        = False           # Whether to use TensorBoard or not

        self.__AllowedDNNTypes     = ["Classification-DNN",
                                      "Z-Reconstruction"]

        #######################################################################
        ############################ Input Options ############################
        #######################################################################
        DNNModel_OS = OptionChecker()
        for item in self._BasicModel__Group:
            DNNModel_OS.CheckDNNModelOption(item)
            if "Nodes" in item:
                self.__Nodes = hf.ReadOption(item, "Nodes", istype="int", islist=True, msg="Could not load 'Nodes'. Make sure you pass a comma-separated list of integer.")
            if "PredesignedModel" in item:
                self.__PredesignedModel = hf.ReadOption(item, "PredesignedModel", istype="str", islist=False, msg="Could not load 'PredesignedModel'. Make sure you pass a string.")
            if "LearningRate" in item:
                self.__LearningRate = hf.ReadOption(item, "LearningRate", istype="float", islist=False, msg="Could not load 'LearningRate'. Make sure you pass a float.")
            if "Patience" in item:
                self.__Patience = hf.ReadOption(item, "Patience", istype="int", islist=False, msg="Could not load 'Patience'. Make sure you pass an integer.")
            if "MinDelta" in item:
                self.__MinDelta = hf.ReadOption(item, "MinDelta", istype="float", islist=False, msg="Could not load 'MinDelta'. Make sure you pass a float.")
            if "DropoutIndece" in item:
                self.__DropoutIndece = hf.ReadOption(item, "DropoutIndece", istype="int", islist=True, msg="Could not load 'DropoutIndece'. Make sure you pass a comma-separated list of integers.")
            if "DropoutProb" in item:
                self.__DropoutProb = hf.ReadOption(item, "DropoutProb", istype="float", islist=False, msg="Could not load 'DropoutProb'. Make sure you pass a float.")
            if "BatchNormIndece" in item:
                self.__BatchNormIndece = hf.ReadOption(item, "BatchNormIndece", istype="int", islist=True, msg="Could not load 'BatchNormIndece'. Make sure you pass a comma-separated list of integers.")
            if "ActivationFunctions" in item:
                self.__ActivationFunctions = hf.ReadOption(item, "ActivationFunctions", istype="str", islist=True)
            if "KernelInitialiser" in item:
                self.__KernelInitialiser = hf.ReadOption(item, "KernelInitialiser", istype="str", islist=False)
            if "KernelRegulariser" in item:
                self.__KernelRegulariser = hf.ReadOption(item, "KernelRegulariser", istype="str", islist=True)
            if "BiasRegulariser" in item:
                self.__BiasRegulariser = hf.ReadOption(item, "BiasRegulariser", istype="str", islist=True)
            if "ActivityRegulariser" in item:
                self.__ActivityRegulariser = hf.ReadOption(item, "ActivityRegulariser", istype="str", islist=True)
            if "OutputSize" in item:
                self.__OutputSize = hf.ReadOption(item, "OutputSize", istype="int", islist=False, msg="Could not load 'OutputSize'. Make sure you pass an integer.") 
            if "OutputActivation" in item:
                self.__OutputActivation = hf.ReadOption(item, "OutputActivation")
            if "Loss" in item:
                self.__Loss = hf.ReadOption(item, "Loss", istype="str", islist=False)
            if "SmoothingAlpha" in item:
                self.__SmoothingAlpha = hf.ReadOption(item, "SmoothingAlpha", istype="float", islist=False)
            if "UseTensorBoard" in item:
                self.__UseTensorBoard = hf.ReadOption(item, "UseTensorBoard", istype="bool", islist=False)

        #############################################################################
        ############################ Check Input Options ############################
        #############################################################################
        if self._BasicModel__Type not in self.__AllowedDNNTypes:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has invalid type set for DNNMODEL!")
        if self._BasicModel__Type!="Classification-DNN" and self.__PredesignedModel!=None:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has predesigned model "+self.__PredesignedModel+" set but type is "+self._BasicModel__Type+". Predesigned models only work for 'Type' Classification-DNN!")
        if self.__PredesignedModel!= None and self.__Nodes != None and is_Train==True:
            WarningMessage("MODEL " + self._BasicModel__Name + " has predesigned model "+self.__PredesignedModel+" set as well as 'Nodes'. 'Nodes' has no effect for predesigned models.")
        if self.__PredesignedModel!= None and self.__DropoutIndece != None and is_Train==True:
            WarningMessage("MODEL " + self._BasicModel__Name + " has predesigned model "+self.__PredesignedModel+" set as well as 'DropoutIndece'. 'DropoutIndece' has no effect for predesigned models.")
        if self.__PredesignedModel!= None and self.__BatchNormIndece != None and is_Train==True:
            WarningMessage("MODEL " + self._BasicModel__Name + " has predesigned model "+self.__PredesignedModel+" set as well as 'BatchNormIndece'. 'BatchNormIndece' has no effect for predesigned models.")
        if self.__Nodes==None and self.__PredesignedModel==None:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has no 'Nodes' defined. You need to pass a comma-separated list of integers to define an architecture or use the 'PredesignedModel' parameter.")
        if self.__Nodes!=None and self.__PredesignedModel!=None and is_Train==True:
            WarningMessage("MODEL " + self._BasicModel__Name + " has both 'Nodes' and 'PredesignedModel' defined. The pre-designed model "+self.__PredesignedModel+" is now used.")
        if self.__MinDelta!=None:
            if self.__MinDelta>=1:
                ErrorMessage("MODEL " + self._BasicModel__Name + " has 'MinDelta' set to "+str(self.__MinDelta)+". 'MinDelta' must be <1.")
        if self.__DropoutProb>=1:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has 'DropoutProb' set to "+str(self.__DropoutProb)+". 'DropoutProb' must be <1.")
        if self.__OutputSize==None and is_Train==True:
            WarningMessage("MODEL " + self._BasicModel__Name + " has no 'OutputSize' set. Setting OutputSize=1.")
            self.__OutputSize = 1
        if self.__ActivationFunctions==None:
            if is_Train==True:
                WarningMessage("MODEL " + self._BasicModel__Name + " has no 'ActivationFunctions' set. Setting all activations functions to 'relu'.")
            self.__ActivationFunctions = ["relu"]*len(self.__Nodes)
        if not set(self.__ActivationFunctions).issubset(["relu", "sigmoid", "softmax", "softplus", "softsign", "tanh", "selu", "elu", "exponential"]) and is_Train==True:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has unknown 'ActivationFunctions' set.")
        if len(self.__ActivationFunctions)!=len(self.__Nodes):
            ErrorMessage("MODEL " + self._BasicModel__Name + " has 'ActivationFunctions' set but length does not correspond to length of 'Nodes' parameter.")
        if self.__KernelInitialiser not in ["glorot_uniform", "glorot_normal", "lecun_normal", "lecun_uniform"]:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has unknown 'KernelInitialiser' set.")
        if self.__KernelRegulariser == None:
            self.__KernelRegulariser = ["None"]*len(self.__Nodes)
        elif not set(self.__KernelRegulariser).issubset(["None","l1", "l2", "l1_l2"]) and is_Train==True:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has unknown 'KernelRegulariser' set.")
        if self.__BiasRegulariser == None:
            self.__BiasRegulariser = ["None"]*len(self.__Nodes)
        elif not set(self.__BiasRegulariser).issubset(["None","l1", "l2", "l1_l2"]) and is_Train==True:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has unknown 'BiasRegulariser' set.")
        if self.__ActivityRegulariser == None:
            self.__ActivityRegulariser = ["None"]*len(self.__Nodes)
        elif not set(self.__ActivityRegulariser).issubset(["None","l1", "l2", "l1_l2"]) and is_Train==True:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has unknown 'ActivityRegulariser' set.")
        if self.__OutputActivation==None and is_Train==True:
            WarningMessage("MODEL " + self._BasicModel__Name + " has no 'OutputActivation' set. Setting OutputActivatation to 'relu'.")
            self.__OutputActivation = "relu"
        if self.__Loss==None:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has no 'Loss' set.")
        elif self.__Loss == "categorical_focal_loss":
            self.__Loss = cl.categorical_focal_loss(gamma=2.0, alpha=0.25)
        elif self.__Loss == "binary_focal_loss":
            self.__Loss = cl.binary_focal_loss(gamma=2.0, alpha=0.25)
        elif self.__Loss not in ["binary_crossentropy", "categorical_crossentropy", "mean_squared_error", "mean_absolute_error", "mean_absolute_percentage_error", "mean_squared_logarithmic_error", "cosine_similarity", "huber_loss", "log_cosh"]:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has unknown or unsupported 'Loss' set: "+self.__Loss)
        if self.__Loss == "binary_crossentropy" and self.__OutputSize != 1:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has 'binary crossentropy' set as 'Loss'. This does not make sense for a model with an outputsize of "+str(self.__OutputSize)+".")
        if self._BasicModel__ClassLabels == None:
            self._BasicModel__ClassLabels = ["Binary"]
        if self._BasicModel__ClassLabels != None and len(self._BasicModel__ClassLabels)!=self.__OutputSize:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has "+str(len(self._BasicModel__ClassLabels))+" class names defined but output size is "+str(self.__OutputSize)+".")
        if self._BasicModel__ClassLabels == None and self.__OutputSize>1:
            ErrorMessage("MODEL " + self._BasicModel__Name + " has 0 class names defined but output size is "+str(self.__OutputSize)+".")
        if self.__SmoothingAlpha != None and (self.__SmoothingAlpha > 0.5 or self.__SmoothingAlpha<0):
            ErrorMessage("MODEL " + self._BasicModel__Name + " has invalid SmoothingAlpha value! Must be between >0 and <0.5!")

    ###########################################################################
    ############################### Misc methods ##############################
    ###########################################################################
    def get_PredesignedModel(self):
        return self.__PredesignedModel

    def get_Nodes(self):
        return self.__Nodes

    def get_OutputSize(self):
        return self.__OutputSize

    def DefineRegulariser(self,Regulariser,l1,l2):
        '''
        Simple function to create a list of keras.regularizers objects
        '''
        Regularisers = []
        for R in Regulariser:
            if R == "l1":
                Regularisers.append(regularizers.l1(l1=l1))
            elif R == "l2":
                Regularisers.append(regularizers.l2(l2=l2))
            elif R == "l1_l2":
                Regularisers.append(regularizers.l1_l2(l1=l1, l2=l2))
            elif R == "None":
                Regularisers.append(None)
        return Regularisers

    def DNNCompile(self):
        '''
        Compile a simple densely connected NN. This is our heavy lifting method.
        It is used whenever the user uses the "Nodes" option in the config file to build
        a densely connected NN.
        '''
        DropoutIndece = self.__DropoutIndece
        BatchNormIndece = self.__BatchNormIndece
        # Set up the regularisers
        KernelRegularisers = self.DefineRegulariser(self.__KernelRegulariser, self.__KernelRegulariserl1, self.__KernelRegulariserl2)
        BiasRegularisers   = self.DefineRegulariser(self.__BiasRegulariser, self.__BiasRegulariserl1, self.__BiasRegulariserl2)
        ActivityRegularisers = self.DefineRegulariser(self.__ActivityRegulariser, self.__ActivityRegulariserl1, self.__ActivityRegulariserl2)
        # Lets build a feed forward NN using keras from scratch
        self.__kerasmodel = Sequential()
        for LayerID, nodes in enumerate(self.__Nodes):
            if LayerID == 0:
                self.__kerasmodel.add(Dense(nodes,
                                            name = "Hidden_Layer_"+str(LayerID),
                                            input_dim=self.__InputDim,
                                            activation=self.__ActivationFunctions[LayerID],
                                            kernel_initializer=self.__KernelInitialiser,
                                            kernel_regularizer=KernelRegularisers[LayerID],
                                            bias_regularizer=BiasRegularisers[LayerID],
                                            activity_regularizer=ActivityRegularisers[LayerID]))
            else:
                self.__kerasmodel.add(Dense(nodes,
                                            name = "Hidden_Layer_"+str(LayerID),
                                            activation=self.__ActivationFunctions[LayerID],
                                            kernel_initializer=self.__KernelInitialiser,
                                            kernel_regularizer=KernelRegularisers[LayerID],
                                            bias_regularizer=BiasRegularisers[LayerID],
                                            activity_regularizer=ActivityRegularisers[LayerID]))
            if(len(DropoutIndece)!=0):
                if LayerID in DropoutIndece:
                    self.__kerasmodel.add(Dropout(self.__DropoutProb)) # Adding a Dropout layers
            if(len(BatchNormIndece)!=0):
                if LayerID in BatchNormIndece:
                    self.__kerasmodel.add(BatchNormalization()) # Adding BatchNormalisation layers
        self.__kerasmodel.add(Dense(self.__OutputSize, activation=self.__OutputActivation, name="Output_Layer"))
        self.__kerasmodel.compile(loss = self.__Loss, optimizer = tf.keras.optimizers.Nadam(learning_rate=self.__LearningRate)
, metrics = self._BasicModel__Metrics)
        self.__kerasmodel.summary()

    def PDNNCompile(self):
        '''
        Compile a custom connected NN with two parallel layers.
        Takes simple list of variables as input.
        '''
        Input_Layer = Input(shape=(self.__InputDim,))
        Dense_Layer = Dense(50, activation = "relu")(Input_Layer)

        # First parallel arm
        Dense_Layer_1   = Dense(50, activation = "relu")(Dense_Layer)
        Dropout_Layer_1 = Dropout(0.2)(Dense_Layer_1)
        Dense_Layer_1   = Dense(50, activation = "relu")(Dropout_Layer_1)

        # Second parallel arm
        Dense_Layer_2   = Dense(50, activation = "relu")(Dense_Layer)
        Dropout_Layer_2 = Dropout(0.2)(Dense_Layer_2)
        Dense_Layer_2   = Dense(50, activation = "relu")(Dropout_Layer_2)

        # Concatenation Layer
        Concat_Layer  = concatenate([Dense_Layer_1, Dense_Layer_2])
        Dense_Layer   = Dense(50, activation='relu')(Concat_Layer)
        Output_Layer  = Dense(self.__OutputSize, activation=self.__OutputActivation,name="Output")(Dense_Layer)
        self.__kerasmodel = keras.Model(inputs=Input_Layer,outputs=Output_Layer)
        self.__kerasmodel.compile(loss = [self.__Loss], optimizer = tf.keras.optimizers.Nadam(learning_rate=self.__LearningRate)
, metrics = self._BasicModel__Metrics)
        self.__kerasmodel.summary()

    def DPDNNCompile(self):
        '''
        Compile a custom connected NN with four parallel layers.
        Takes simple list of variables as input.
        '''
        Input_Layer = Input(shape=(self.__InputDim,))
        Dense_Layer = Dense(50, activation = "relu")(Input_Layer)

        # First parallel arm
        Dense_Layer_1   = Dense(50, activation = "relu")(Dense_Layer)
        Dropout_Layer_1 = Dropout(0.2)(Dense_Layer_1)
        Dense_Layer_1   = Dense(50, activation = "relu")(Dropout_Layer_1)

        # Second parallel arm
        Dense_Layer_2   = Dense(50, activation = "relu")(Dense_Layer)
        Dropout_Layer_2 = Dropout(0.2)(Dense_Layer_2)
        Dense_Layer_2   = Dense(50, activation = "relu")(Dropout_Layer_2)

        # Third parallel arm
        Dense_Layer_3   = Dense(50, activation = "relu")(Dense_Layer)
        Dropout_Layer_3 = Dropout(0.2)(Dense_Layer_3)
        Dense_Layer_3   = Dense(50, activation = "relu")(Dropout_Layer_3)

        # Fourth parallel arm
        Dense_Layer_4   = Dense(50, activation = "relu")(Dense_Layer)
        Dropout_Layer_4 = Dropout(0.2)(Dense_Layer_4)
        Dense_Layer_4   = Dense(50, activation = "relu")(Dropout_Layer_4)

        # Concatenation Layer
        Concat_Layer  = concatenate([Dense_Layer_1, Dense_Layer_2, Dense_Layer_3, Dense_Layer_4])
        Dense_Layer   = Dense(50, activation='relu')(Concat_Layer)
        Output_Layer  = Dense(self.__OutputSize, activation=self.__OutputActivation,name="Output")(Dense_Layer)
        self.__kerasmodel = keras.Model(inputs=Input_Layer,outputs=Output_Layer)
        self.__kerasmodel.compile(loss = [self.__Loss], optimizer = tf.keras.optimizers.Nadam(learning_rate=self.__LearningRate)
, metrics = self._BasicModel__Metrics)
        self.__kerasmodel.summary()

    def PADNNCompile(self):
        '''
        Compile a custom connected NN with two parallel layers including attention.
        Takes simple list of variables as input.
        '''
        Input_Layer = Input(shape=(self.__InputDim,))
        Dense_Layer = Dense(50, activation = "relu")(Input_Layer)

        # First parallel arm
        Dense_Layer_1          = Dense(50, activation = "relu")(Dense_Layer)
        Attention_Prob_Layer_1 = Dense(50, activation = "sigmoid")(Dense_Layer_1)
        Attention_Layer_1      = multiply([Dense_Layer_1, Attention_Prob_Layer_1])
        Dropout_Layer_1        = Dropout(0.2)(Attention_Layer_2)
        Dense_Layer_1          = Dense(50, activation = "relu")(Dropout_Layer_1)

        # Second parallel arm
        Dense_Layer_2          = Dense(50, activation = "relu")(Dense_Layer)
        Attention_Prob_Layer_2 = Dense(50, activation = "sigmoid")(Dense_Layer_2)
        Attention_Layer_2      = multiply([Dense_Layer_2, Attention_Prob_Layer_2])
        Dropout_Layer_2        = Dropout(0.2)(Attention_Layer_2)
        Dense_Layer_2          = Dense(50, activation = "relu")(Dropout_Layer_2)

        # Concatenation Layer
        Concat_Layer  = concatenate([Dense_Layer_1, Dense_Layer_2])
        Dense_Layer   = Dense(50, activation='relu')(Concat_Layer)
        Output_Layer  = Dense(self.__OutputSize, activation=self.__OutputActivation,name="Output")(Dense_Layer)
        self.__kerasmodel = keras.Model(inputs=Input_Layer,outputs=Output_Layer)
        self.__kerasmodel.compile(loss = [self.__Loss], optimizer = tf.keras.optimizers.Nadam(learning_rate=self.__LearningRate)
, metrics = self._BasicModel__Metrics)
        self.__kerasmodel.summary()

    def DPADNNCompile(self):
        '''
        Compile a custom connected NN with Four parallel layers including attention.
        Takes simple list of variables as input.
        '''
        Input_Layer = Input(shape=(self.__InputDim,))
        Dense_Layer = Dense(50, activation = "relu")(Input_Layer)

        # First parallel arm
        Dense_Layer_1          = Dense(50, activation = "relu")(Dense_Layer)
        Attention_Prob_Layer_1 = Dense(50, activation = "sigmoid")(Dense_Layer_1)
        Attention_Layer_1      = multiply([Dense_Layer_1, Attention_Prob_Layer_1])
        Dropout_Layer_1        = Dropout(0.2)(Attention_Layer_1)
        Dense_Layer_1          = Dense(50, activation = "relu")(Dropout_Layer_1)

        # Second parallel arm
        Dense_Layer_2          = Dense(50, activation = "relu")(Dense_Layer)
        Attention_Prob_Layer_2 = Dense(50, activation = "sigmoid")(Dense_Layer_2)
        Attention_Layer_2      = multiply([Dense_Layer_2, Attention_Prob_Layer_2])
        Dropout_Layer_2        = Dropout(0.2)(Attention_Layer_2)
        Dense_Layer_2          = Dense(50, activation = "relu")(Dropout_Layer_2)

        # Third parallel arm
        Dense_Layer_3          = Dense(50, activation = "relu")(Dense_Layer)
        Attention_Prob_Layer_3 = Dense(50, activation = "sigmoid")(Dense_Layer_3)
        Attention_Layer_3      = multiply([Dense_Layer_3, Attention_Prob_Layer_3])
        Dropout_Layer_3        = Dropout(0.2)(Attention_Layer_3)
        Dense_Layer_3          = Dense(50, activation = "relu")(Dropout_Layer_3)

        # Fourth parallel arm
        Dense_Layer_4          = Dense(50, activation = "relu")(Dense_Layer)
        Attention_Prob_Layer_4 = Dense(50, activation = "sigmoid")(Dense_Layer_4)
        Attention_Layer_4      = multiply([Dense_Layer_4, Attention_Prob_Layer_4])
        Dropout_Layer_4        = Dropout(0.2)(Attention_Layer_4)
        Dense_Layer_4          = Dense(50, activation = "relu")(Dropout_Layer_4)

        # Concatenation Layer
        Concat_Layer  = concatenate([Dense_Layer_1, Dense_Layer_2, Dense_Layer_3, Dense_Layer_4])
        Dense_Layer   = Dense(50, activation='relu')(Concat_Layer)
        Output_Layer  = Dense(self.__OutputSize, activation=self.__OutputActivation,name="Output")(Dense_Layer)
        self.__kerasmodel = keras.Model(inputs=Input_Layer,outputs=Output_Layer)
        self.__kerasmodel.compile(loss = [self.__Loss], optimizer = tf.keras.optimizers.Nadam(learning_rate=self.__LearningRate)
, metrics = self._BasicModel__Metrics)
        self.__kerasmodel.summary()

    def ResDNNCompile(self):
        '''
        Compile a custom connected NN with Four parallel layers including attention.
        Takes simple list of variables as input.
        '''
        Input_Layer = Input(shape=(self.__InputDim,))
        Dense_Layer = Dense(50, activation = "relu")(Input_Layer)

        Shortcut    = Dense_Layer
        Dense_Layer = Dense(50, activation = "relu")(Dense_Layer)
        BatchNorm_Layer = BatchNormalization()(Dense_Layer)
        Dense_Layer = Dense(50, activation = "relu")(BatchNorm_Layer)
        Dropout_Layer = Dropout(0.2)(Dense_Layer)
        Add_Layer   = Add()([Dropout_Layer, Shortcut])
        Dense_Layer = Dense(50, activation = "relu")(Add_Layer)

        Shortcut    = Dense_Layer
        Dense_Layer = Dense(50, activation = "relu")(Dense_Layer)
        BatchNorm_Layer = BatchNormalization()(Dense_Layer)
        Dense_Layer = Dense(50, activation = "relu")(BatchNorm_Layer)
        Dropout_Layer = Dropout(0.2)(Dense_Layer)
        Add_Layer   = Add()([Dropout_Layer, Shortcut])
        Dense_Layer = Dense(50, activation = "relu")(Add_Layer)

        Output_Layer  = Dense(self.__OutputSize, activation=self.__OutputActivation,name="Output")(Add_Layer)
        self.__kerasmodel = keras.Model(inputs=Input_Layer,outputs=Output_Layer)
        self.__kerasmodel.compile(loss = [self.__Loss], optimizer = tf.keras.optimizers.Nadam(learning_rate=self.__LearningRate)
, metrics = self._BasicModel__Metrics)
        self.__kerasmodel.summary()

    ########################################################################
    ############################ Model Training ############################
    ########################################################################

    def DNNTrain(self, X, Y, W, output_path, Fold="", Variables = None):
        '''
        Training a simple densely connected NN. This method serves as the "heavy lifter" for training.
        It should work well for not to complicated inputs/outputs.

        Keyword Arguments:
        X           --- Array of data to be used for training
        Y           --- Labels
        W           --- Weights
        output_path --- Output path where the model.h5 files and the history is to be saved
        Fold        --- String declaring the fold name. This will be appended to the modelname upon saving it.
        '''
        if(Fold!=""):
            Fold = "_Fold"+Fold
        # Definition of Callbacks
        callbacks = []
        if self.__UseTensorBoard == True:
            tb = TensorBoard(log_dir=hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+"_TensorBoardLog"+Fold,
                             histogram_freq=5)
            callbacks.append(tb)
        if self.__Patience != None and self.__MinDelta!= None:
            es = EarlyStopping(monitor='val_loss', min_delta=self.__MinDelta, patience = self.__Patience, restore_best_weights=True)
            callbacks.append(es)
        seed = np.random.randint(1000, size=1)[0]
        W_train, W_val, _, _ = train_test_split(W, Y, test_size=self._BasicModel__ValidationSize, random_state=seed)
        X_train, X_val, y_train, y_val = train_test_split(X, Y, test_size=self._BasicModel__ValidationSize, random_state=seed)

        TrainerMessage("Starting Training...")
        if(len(callbacks)!=0):
            history = self.__kerasmodel.fit(X_train, y_train, epochs=self._BasicModel__Epochs, batch_size=self._BasicModel__BatchSize, verbose=1, sample_weight=W_train, validation_data = (X_val, y_val, W_val), callbacks = callbacks)
        else:
            history = self.__kerasmodel.fit(X_train, y_train, epochs=self._BasicModel__Epochs, batch_size=self._BasicModel__BatchSize, verbose=1, sample_weight=W_train, validation_data = (X_val, y_val, W_val))
        TrainerDoneMessage("Training done! Writing model to "+ hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+Fold+".h5")
        self.__kerasmodel.save(hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+Fold+".h5")
        TrainerDoneMessage("Writing history to "+ hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+"_history"+Fold+".json")
        hist_df = pd.DataFrame(history.history)
        with open(hf.ensure_trailing_slash(output_path)+self._BasicModel__Name+"_"+self._BasicModel__Type+"_history"+Fold+".json", mode='w') as f:
            hist_df.to_json(f)

    def CompileAndTrain(self, X, Y, W, output_path, Fold="", Variables=None):
        '''
        Combination of compilation and training for a densely connected NN.

        Keyword Arguments:
        X           --- Array of data to be used for training
        Y           --- Labels
        W           --- Weights
        output_path --- Output path where the model.h5 files and the history is to be saved
        Fold        --- String declaring the fold name. This will be appended to the modelname upon saving it.
        '''
        if(len(self.get_ClassLabels())>2 and self.__Loss == "categorical_crossentropy"):
            Y=np.array(hf.OneHot(Y, len(self.get_ClassLabels()))).astype(np.float32)
        else:
            Y=np.array(Y).astype(np.float32)
        if self.__SmoothingAlpha!=None:
            Y=self.smooth_labels(Y,K=self.__OutputSize,alpha=self.__SmoothingAlpha)
        # Pre-designed paralell DNN
        if(self.__PredesignedModel=="PDNN"):
            self.PDNNCompile()
        # Pre-designed double paralell DNN
        elif(self.__PredesignedModel=="DPDNN"):
            self.DPDNNCompile()
        # Pre-designed paralell DNN with attention
        elif(self.__PredesignedModel=="PADNN"):
            self.PADNNCompile()
        # Pre-designed double paralell DNN with attention
        elif(self.__PredesignedModel=="DPADNN"):
            self.DPADNNCompile()
        # Pre-designed ResDNN
        elif(self.__PredesignedModel=="ResDNN"):
            self.ResDNNCompile()
        # "Normal" densely connected DNN
        else:
            self.DNNCompile()
        self.DNNTrain(X, Y, W, output_path, Fold, Variables)

    ##############################################################################
    ############################ Printing Information ############################
    ##############################################################################

    def __str__(self):
        '''
        Method to represent the class objects as a string.
        '''
        Nodes = ",".join([str(int) for int in self.__Nodes])
        DropoutIndece = ",".join([str(int) for int in self.__DropoutIndece]) if len(self.__DropoutIndece) != 0 else "None"
        BatchNormIndece = ",".join([str(int) for int in self.__BatchNormIndece]) if len(self.__BatchNormIndece) != 0 else "None"

        return ("MODELINFO:\tName: "+self._BasicModel__Name
                +"\nMODELINFO:\tType: "+self._BasicModel__Type
                +"\nMODELINFO:\tPredesignedModel: "+str(self.__PredesignedModel)
                +"\nMODELINFO:\tNodes: "+Nodes
                +"\nMODELINFO:\tEpochs: "+str(self._BasicModel__Epochs)
                +"\nMODELINFO:\tBatchSize: "+str(self._BasicModel__BatchSize)
                +"\nMODELINFO:\tPatience: "+str(self.__Patience)
                +"\nMODELINFO:\tMinDelta: "+str(self.__MinDelta)
                +"\nMODELINFO:\tDropoutIndece: "+DropoutIndece
                +"\nMODELINFO:\tDropoutProb: "+str(self.__DropoutProb)
                +"\nMODELINFO:\tBatchNormIndece: "+BatchNormIndece
                +"\nMODELINFO:\tOutputSize: "+str(self.__OutputSize)
                +"\nMODELINFO:\tOutputActivation: "+self.__OutputActivation
                +"\nMODELINFO:\tLoss: "+self.__Loss
                +"\nMODELINFO:\tMetrics: "+str(self._BasicModel__Metrics))

    def Print(self):
        '''
        Simple print method.
        '''
        self.__kerasmodel.Print()
