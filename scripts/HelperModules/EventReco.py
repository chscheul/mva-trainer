from ROOT import TLorentzVector
from HelperModules.MessageHandler import ErrorMessage
import numpy as np

# some variables which are necessary for truth-reco matching
RecoVariables = ["pT_1jet","pT_2jet","pT_3jet","pT_4jet",
                 "e_1jet","e_2jet","e_3jet","e_4jet",
                 "eta_1jet","eta_2jet","eta_3jet","eta_4jet",
                 "phi_1jet","phi_2jet","phi_3jet","phi_4jet",
                 "pT_1lep","pT_2lep","pT_3lep","pT_4lep",
                 "e_1lep","e_2lep","e_3lep","e_4lep",
                 "eta_1lep","eta_2lep","eta_3lep","eta_4lep",
                 "phi_1lep","phi_2lep","phi_3lep","phi_4lep",
                 "charge_1lep", "charge_2lep", "charge_3lep", "charge_4lep",
                 "type_1lep", "type_2lep", "type_3lep", "type_4lep",
                 "eventNumber"]
TruthVariables = ["MC_b_from_t_pt","MC_b_from_tbar_pt",
                  "MC_b_from_t_m","MC_b_from_tbar_m",
                  "MC_b_from_t_eta","MC_b_from_tbar_eta",
                  "MC_b_from_t_phi","MC_b_from_tbar_phi",
                  "MC_Wdecay1_from_t_pdgId","MC_Wdecay1_from_tbar_pdgId",
                  "MC_Wdecay2_from_t_pdgId","MC_Wdecay2_from_tbar_pdgId",
                  "MC_Wdecay1_from_t_pt","MC_Wdecay1_from_tbar_pt",
                  "MC_Wdecay2_from_t_pt","MC_Wdecay2_from_tbar_pt",
                  "MC_Wdecay1_from_t_eta","MC_Wdecay1_from_tbar_eta",
                  "MC_Wdecay2_from_t_eta","MC_Wdecay2_from_tbar_eta",
                  "MC_Wdecay1_from_t_phi","MC_Wdecay1_from_tbar_phi",
                  "MC_Wdecay2_from_t_phi","MC_Wdecay2_from_tbar_phi",
                  "MC_Wdecay1_from_t_m","MC_Wdecay1_from_tbar_m",
                  "MC_Wdecay2_from_t_m","MC_Wdecay2_from_tbar_m",
                  "MC_Zdecay1_pt","MC_Zdecay2_pt",
                  "MC_Zdecay1_eta","MC_Zdecay2_eta",
                  "MC_Zdecay1_phi","MC_Zdecay2_phi",
                  "MC_Zdecay1_m","MC_Zdecay2_m",
                  "MC_Zdecay1_pdgId","MC_Zdecay2_pdgId",
                  "eventNumber"]

def MatchRecoTruth(reco_indece, truth_vec_inf, reco_vec_inf, DR_cut):
    """
    This is an algorithm that performs reco-truth matching  based on a deltaR cut

    Keyword Arguments:
    truth_vec_inf   --- list of tuples of TLorentzVectors charge and flavour representing truth objects
    reco_indece     --- list of integers representing truth indece
    reco_vec_inf    --- list of tuples of TLorentzVectors charge and flavour representing reco objects
    DR_cut          --- maximal Delta R between two objects to be matched
    """
    if (len(truth_vec_inf) == 0 or len(reco_vec_inf) == 0): return [-1,-1,-1,-1]
    DeltaR_matrix = np.ma.array(CalculateDeltaRMatrix.outer(reco_vec_inf, truth_vec_inf))
    DeltaR_matrix = np.ma.masked_greater(DeltaR_matrix, DR_cut)
    inverse_mask = CheckDoublematches(DeltaR_matrix, len(truth_vec_inf), DR_cut)
    reco_indece = inverse_mask.dot(reco_indece) - 1
    return reco_indece

def CheckDoublematches(DeltaR_matrix, dim, DR_cut):
    """
    Function to check, if there is one lepton which can me matched with multiple leptons.
    If there is a doubke match, choose the match with the smaller Delta R

    Keyword Arguments:
    DeltaR_matrix   --- matrix containing all DeltaR values of all objects with Delta R < DR_cut and fulfilling flavour/charge requirements
    dim             --- n of an nxm matrix
    DR_cut          --- maximal Delta R between two objects to be matched
    """
    inverse_mask = ~DeltaR_matrix.mask
    check_matches = np.nonzero(inverse_mask)
    if(len(check_matches[0]) != len(np.unique(check_matches[0])) or len(check_matches[1]) != len(np.unique(check_matches[1]))):
        minimal_deltaR = int(np.argmin(DeltaR_matrix))
        min_argx, min_argy = DetermineIndece(minimal_deltaR, dim)
        DeltaR_matrix[min_argy,:] = np.ma.masked
        DeltaR_matrix[:, min_argx] = np.ma.masked
        DeltaR_matrix[min_argy, min_argx] = DR_cut
        inverse_mask= ~DeltaR_matrix.mask
        return CheckDoublematches(DeltaR_matrix, dim, DR_cut)
    else:
        return inverse_mask

def TryInvMassApproach(reco_vec_inf, reco_indece):
    """
    Function to check for leptons, if the invariant mass approach leads to same result as the truth matching approach
    for the Z boson reconstruction

    Keyword Arguments:
    reco_vec_inf    --- list of tuples of TLorentzVectors charge and flavour representing reco objects
    reco_indece     --- list of truth-reco leptons pairs: the value gives the thruth lepton, the index the reco lepton
    """
    if (0 in reco_indece and 1 in reco_indece):
        DeltaMass_matrix = CalculateDeltaMassMatrix.outer(reco_vec_inf, reco_vec_inf)
        arg_minimal_mass = np.argmin(DeltaMass_matrix)
        lep_1_ind, lep_2_ind = DetermineIndece(arg_minimal_mass, len(reco_vec_inf))
        if (reco_indece[lep_1_ind] in [0,1] and reco_indece[lep_2_ind] in [0,1]):
            return 0
        elif (not(reco_indece[lep_1_ind] in [0,1]) and not(reco_indece[lep_2_ind] in [0,1])):
            return 1
        elif (reco_indece[lep_1_ind] in [0,1] and not(reco_indece[lep_2_ind] in [0,1])) or (not(reco_indece[lep_1_ind] in [0,1]) and reco_indece[lep_2_ind] in [0,1]):
            return 2
    else:
        return -2

def DeltaRMatrix(reco_vec_inf, truth_vec_inf):
    """
    Calculate the DeltaR between a reco and a truth object

    Keyword Arguments:
    truth_vec_inf   --- list of tuples of TLorentzVectors charge and flavour representing truth objects
    reco_vec_inf    --- list of tuples of TLorentzVectors charge and flavour representing reco objects
    """
    if (truth_vec_inf[1] == reco_vec_inf[1] and truth_vec_inf[2] == reco_vec_inf[2]):
        DeltaR = truth_vec_inf[0].DeltaR(reco_vec_inf[0])
    else:
        DeltaR = 9999.
    return DeltaR

#Define DeltaRMatrix as function used for an outer product. It takes two 1D arrays as input and return a 2D array
CalculateDeltaRMatrix = np.frompyfunc(DeltaRMatrix, 2, 1)

def DeltaMassMatrix(reco_vec_inf_1, reco_vec_inf_2):
    """
    Calculate the invariant mass of two reco objects and determine the difference to m_Z

    Keyword Arguments:
    reco_vec_inf    --- list of tuples of TLorentzVectors charge and flavour representing reco objects
    """
    m_Z = 91.1876
    if (reco_vec_inf_1[1] != reco_vec_inf_2[1] and reco_vec_inf_1[2] == reco_vec_inf_2[2]):
       invmass_vec = reco_vec_inf_1[0] + reco_vec_inf_2[0]
       Delta_mass = abs(invmass_vec.M()-m_Z)
    else:
        Delta_mass = 9999.
    return Delta_mass

#Define DeltaMassMatrix as function used for an outer product. It takes two 1D arrays as input and return a 2D array
CalculateDeltaMassMatrix = np.frompyfunc(DeltaMassMatrix, 2, 1)

def DetermineIndece(arg, dim = 4):
    """
    calculate 1D indece of matrix with Dimension = dim into 2D indece

    ---- arguments: ----
    arg : 1D indece
    dim : dimension of matrix

    """
    x_arg = arg % dim
    y_arg = arg // dim
    return x_arg, y_arg

class RecoObject(object):
    """
    Class representing reconstructed objects such as charged leptons or jets. The purpose is to easily get their 4-vectors.

    Keyword arguments:
    pt      --- Transverse momentum
    eta     --- Pseudorapidity
    phi     --- Azimuthal angle
    e       --- Energy
    charge  --- Charge
    flavour --- absolute value of pdgID
    """
    def __init__(self, pt, eta, phi, e, charge, flavour):
        self.__pt       = pt
        self.__phi      = phi
        self.__eta      = eta
        self.__e        = e
        self.__v        = TLorentzVector()
        self.__v.SetPtEtaPhiE(self.__pt, self.__eta, self.__phi, self.__e)
        self.__charge   = charge
        self.__flavour  = flavour

    def Vector(self):
        return self.__v
    
    def Charge(self):
        return self.__charge

    def Type(self):
        return self.__flavour

class TruthObject(object):
    """
    Class representing truth objects such as leptons or jets. The purpose is to easily get their 4-vectors.

    Keyword arguments:
    pt      --- Transverse momentum in MeV
    eta     --- Pseudorapidity
    phi     --- Azimuthal angle
    m       --- Mass in MeV
    charge  --- Charge
    flavour --- absolute value of pdgID
    """
    def __init__(self, pt, eta, phi, m, pdgID):
        self.__pt       = np.float32(pt/1000) # Need to convert to GeV
        self.__phi      = np.float32(phi)
        self.__eta      = np.float32(eta)
        self.__m        = np.float32(m/1000) # Need to convert to GeV
        self.__v        = TLorentzVector()
        self.__v.SetPtEtaPhiM(self.__pt, self.__eta, self.__phi, self.__m)
        # if the matching should not depend on charge/flavour, set the pdgID to 999
        if pdgID == 999:
            self.charge = pdgID
        else:
            self.__charge   = -np.sign(pdgID)
        self.__flavour  = abs(pdgID)

    def Vector(self):
        return self.__v

    def Charge(self):
        return self.__charge
    
    def Type(self):
        return self.__flavour

class Event(object):
    """
    Class for event reconstruction.

    Keyword arguments:
    VariablesDict --- Dictionary holding all variables necessary for event reconstruction and definition.
    """
    def __init__(self, VariablesDict, Model):
        self.__VariablesDict = VariablesDict
        self.__NClasses      = len(Model.get_ClassLabels())
        self.__JetIndeceDict = {"b_from_t"     : 0,
                                "b_from_tbar"  : 1,
                                "q1_from_t"    : 2,
                                "q2_from_t"    : 3,
                                "q1_from_tbar" : 4,
                                "q1_from_tbar" : 5}
        self.__LeptonIndeceDict = {"l1_from_Z"   : 0,
                                   "l2_from_Z"   : 1,
                                   "l_from_t"    : 2,
                                   "l_from_tbar" : 3}

        self.__Z_matched            = False
        self.__Z_half_matched       = False
        self.__l_from_t_matched     = False
        self.__l_from_tbar_matched  = False
        self.__b_from_t_matched     = False
        self.__b_from_tbar_matched  = False
        self.__q1_from_t_matched    = False
        self.__q1_from_tbar_matched = False
        self.__q2_from_t_matched    = False
        self.__q2_from_tbar_matched = False

        # For sanity check
        self.__isLeptonic = False
        self.__isDilepton = False
        self.__isFullHadr = False
        self.__isUndef    = False

        self.__isLeptonicReco = False
        self.__isDileptonReco = False

        self.__RecoJets  = {str(i)+"jet": RecoObject(self.__VariablesDict["pT_"+str(i)+"jet"],self.__VariablesDict["eta_"+str(i)+"jet"],self.__VariablesDict["phi_"+str(i)+"jet"],self.__VariablesDict["e_"+str(i)+"jet"], 999, 999) if self.__VariablesDict["pT_"+str(i)+"jet"]!=0 else None for i in range(1,5)}
        self.__RecoLeptons  = {str(i)+"lep": RecoObject(self.__VariablesDict["pT_"+str(i)+"lep"],self.__VariablesDict["eta_"+str(i)+"lep"],self.__VariablesDict["phi_"+str(i)+"lep"],self.__VariablesDict["e_"+str(i)+"lep"],self.__VariablesDict["charge_"+str(i)+"lep"], self.__VariablesDict["type_"+str(i)+"lep"]) if self.__VariablesDict["pT_"+str(i)+"lep"]!=0 else None for i in range(1,5)}

        if len(self.__RecoLeptons) == 3:
            self.__isLeptonicReco = True
        elif len(self.__RecoLeptons) == 4:
            self.__isDileptonReco = True

        self.__TruthJets = {}
        self.__TruthLeptons = {}

        lepton_counter = 0
        ####################################################################
        ###################### b and Z reconstruction ######################
        ####################################################################
        self.__TruthLeptons["l1_from_Z"] = TruthObject(self.__VariablesDict["MC_Zdecay1_pt"],self.__VariablesDict["MC_Zdecay1_eta"],self.__VariablesDict["MC_Zdecay1_phi"],self.__VariablesDict["MC_Zdecay1_m"], self.__VariablesDict["MC_Zdecay1_pdgId"])
        self.__TruthLeptons["l2_from_Z"] = TruthObject(self.__VariablesDict["MC_Zdecay2_pt"],self.__VariablesDict["MC_Zdecay2_eta"],self.__VariablesDict["MC_Zdecay2_phi"],self.__VariablesDict["MC_Zdecay2_m"], self.__VariablesDict["MC_Zdecay2_pdgId"])
        self.__TruthJets["b_from_t"] = TruthObject(self.__VariablesDict["MC_b_from_t_pt"],self.__VariablesDict["MC_b_from_t_eta"],self.__VariablesDict["MC_b_from_t_phi"],self.__VariablesDict["MC_b_from_t_m"], 999)
        self.__TruthJets["b_from_tbar"] = TruthObject(self.__VariablesDict["MC_b_from_tbar_pt"],self.__VariablesDict["MC_b_from_tbar_eta"],self.__VariablesDict["MC_b_from_tbar_phi"],self.__VariablesDict["MC_b_from_tbar_m"], 999)

        ####################################################################
        ###################### t side reconstruction #######################
        ####################################################################
        if(abs(self.__VariablesDict["MC_Wdecay1_from_t_pdgId"]) in [1,2,3,4,5,6]):
            self.__TruthJets["q1_from_t"] = TruthObject(self.__VariablesDict["MC_Wdecay1_from_t_pt"],self.__VariablesDict["MC_Wdecay1_from_t_eta"],self.__VariablesDict["MC_Wdecay1_from_t_phi"],self.__VariablesDict["MC_Wdecay1_from_t_m"], 999)
            self.__TruthJets["q2_from_t"] = TruthObject(self.__VariablesDict["MC_Wdecay2_from_t_pt"],self.__VariablesDict["MC_Wdecay2_from_t_eta"],self.__VariablesDict["MC_Wdecay2_from_t_phi"],self.__VariablesDict["MC_Wdecay2_from_t_m"], 999)
            self.__TruthLeptons["l_from_t"] = None 
        elif (abs(self.__VariablesDict["MC_Wdecay1_from_t_pdgId"]) in [11,12,13,14,15,16]):
            self.__TruthJets["q1_from_t"] = None
            self.__TruthJets["q2_from_t"] = None
            if(abs(self.__VariablesDict["MC_Wdecay1_from_t_pdgId"]) in [11,13,15]):
                self.__TruthLeptons["l_from_t"] = TruthObject(self.__VariablesDict["MC_Wdecay1_from_t_pt"],self.__VariablesDict["MC_Wdecay1_from_t_eta"],self.__VariablesDict["MC_Wdecay1_from_t_phi"],self.__VariablesDict["MC_Wdecay1_from_t_m"], self.__VariablesDict["MC_Wdecay1_from_t_pdgId"])
                lepton_counter+=1
            elif(abs(self.__VariablesDict["MC_Wdecay2_from_t_pdgId"]) in [11,13,15]):
                self.__TruthLeptons["l_from_t"] = TruthObject(self.__VariablesDict["MC_Wdecay2_from_t_pt"],self.__VariablesDict["MC_Wdecay2_from_t_eta"],self.__VariablesDict["MC_Wdecay2_from_t_phi"],self.__VariablesDict["MC_Wdecay2_from_t_m"], self.__VariablesDict["MC_Wdecay2_from_t_pdgId"])
                lepton_counter+=1
        else:
            self.__isUndef = True
            self.__TruthJets["q1_from_t"] = None
            self.__TruthJets["q2_from_t"] = None
            self.__TruthLeptons["l_from_t"] = None
                
        ####################################################################
        #################### tbar side reconstruction ######################
        ####################################################################
        if(abs(self.__VariablesDict["MC_Wdecay1_from_tbar_pdgId"]) in [1,2,3,4,5,6]):
            self.__TruthJets["q1_from_tbar"] = TruthObject(self.__VariablesDict["MC_Wdecay1_from_tbar_pt"],self.__VariablesDict["MC_Wdecay1_from_tbar_eta"],self.__VariablesDict["MC_Wdecay1_from_tbar_phi"],self.__VariablesDict["MC_Wdecay1_from_tbar_m"], 999)
            self.__TruthJets["q2_from_tbar"] = TruthObject(self.__VariablesDict["MC_Wdecay2_from_tbar_pt"],self.__VariablesDict["MC_Wdecay2_from_tbar_eta"],self.__VariablesDict["MC_Wdecay2_from_tbar_phi"],self.__VariablesDict["MC_Wdecay2_from_tbar_m"], 999)
            self.__TruthLeptons["l_from_tbar"] = None 
        elif (abs(self.__VariablesDict["MC_Wdecay1_from_tbar_pdgId"]) in [11,12,13,14,15,16]):
            self.__TruthJets["q1_from_tbar"] = None
            self.__TruthJets["q2_from_tbar"] = None
            if(abs(self.__VariablesDict["MC_Wdecay1_from_tbar_pdgId"]) in [11,13,15]):
                self.__TruthLeptons["l_from_tbar"] = TruthObject(np.float32(self.__VariablesDict["MC_Wdecay1_from_tbar_pt"]),np.float32(self.__VariablesDict["MC_Wdecay1_from_tbar_eta"]),np.float32(self.__VariablesDict["MC_Wdecay1_from_tbar_phi"]),np.float32(self.__VariablesDict["MC_Wdecay1_from_tbar_m"]), self.__VariablesDict["MC_Wdecay1_from_tbar_pdgId"])
                lepton_counter+=1
            elif(abs(self.__VariablesDict["MC_Wdecay2_from_tbar_pdgId"]) in [11,13,15]):
                self.__TruthLeptons["l_from_tbar"] = TruthObject(np.float32(self.__VariablesDict["MC_Wdecay2_from_tbar_pt"]),np.float32(self.__VariablesDict["MC_Wdecay2_from_tbar_eta"]),np.float32(self.__VariablesDict["MC_Wdecay2_from_tbar_phi"]),np.float32(self.__VariablesDict["MC_Wdecay2_from_tbar_m"]), self.__VariablesDict["MC_Wdecay2_from_tbar_pdgId"])
                lepton_counter+=1
        else:
            self.__isUndef = True
            self.__TruthJets["q1_from_tbar"] = None
            self.__TruthJets["q2_from_tbar"] = None
            self.__TruthLeptons["l_from_tbar"] = None

        if(lepton_counter == 0):
            self.__isFullHadr = True
        if(lepton_counter == 1):
            self.__isLeptonic = True
        if(lepton_counter == 2):
            self.__isDilepton = True

        # Sanity check to make sure event is fullfilling exactly one category
        EventType = iter([self.__isFullHadr, self.__isLeptonic, self.__isDilepton])
        if not(any(EventType) and not any(EventType)) and not self.__isUndef:
            print("error")
            # Add error message here
            exit()
    

    def MatchWithTruth(self, nItems, option="Jets"):
        '''
        Method to match reco objects with truth objects.
        Keyword Arguments:
        nItems --- Number of items to be matched (e.g. 3 only consider first three objects)
        option --- Can be either "Jets" or "Leptons"
        '''
        if(option=="Jets"):
            # Truth jets
            b_from_t     = self.__TruthJets["b_from_t"]
            b_from_tbar  = self.__TruthJets["b_from_tbar"]
            q1_from_t    = self.__TruthJets["q1_from_t"]
            q2_from_t    = self.__TruthJets["q2_from_t"]
            q1_from_tbar = self.__TruthJets["q1_from_tbar"]
            q2_from_tbar = self.__TruthJets["q2_from_tbar"]

            RecoJetsVectors = [item.Vector() for key,item in self.__RecoJets.items() if item !=None]
            RecoVectorsInfos = np.empty(len(RecoVectors), dtype = [("p4", TLorentzVector), ("charge", np.int32), ("flavour", np.int32)])
            RecoJetsVectorsInfos["p4"]        = RecoJetsVectors
            RecoJetsVectorsInfos["charge"]    = [item.Charge() for key, item in self.__RecoJets.items() if item != None]
            RecoJetsVectorsInfos["flavour"]   = [item.Type() for key,item in self.__RecoJets.items() if item !=None]

            TruthJetsVectors = [self.__TruthJets[key].Vector() for key,value in self.__JetIndeceDict.items() if self.__TruthJets[key]!=None]
            TruthJetsVectorsInfos = np.empty(len(TruthVectors), dtype = [("p4", TLorentzVector), ("charge", np.int32), ("flavour", np.int32)])
            TruthJetsVectorsInfos["p4"]       = TruthJetsVectors
            TruthJetsVectorsInfos["charge"]   = [self.__TruthJets[key].Charge() for key,value in self.__JetIndeceDict.items() if self.__TruthJets[key]!=None]
            TruthJetsVectorsInfos["flavour"]  = [self.__TruthJets[key].Type() for key,value in self.__JetIndeceDict.items() if self.__TruthJets[key]!=None]

            RecoJetIndece = MatchRecoTruth([value+1 for key,value in self.__LeptonIndeceDict.items() if self.__TruthLeptons[key]!=None], TruthJetsVectorsInfos, RecoJetsVectorsInfos, DR_cut = 0.3) # Here the matching happens for jets

            if(0 in RecoJetIndece):
                self.__b_from_t_matched = True
            if(1 in RecoJetIndece):
                self.__b_from_tbar_matched = True
            if(2 in RecoJetIndece):
                self.__q1_from_t_matched = True
            if(3 in RecoJetIndece):
                self.__q2_from_t_matched = True
            if(4 in RecoJetIndece):
                self.__q1_from_tbar_matched = True
            if(5 in RecoJetIndece):
                self.__q2_from_tbar_matched = True

            if(nItems > len(RecoJetIndece)):
               RecoJetIndece.extend([-2] * (nItems - len(RecoJetIndece)))
            return list(RecoJetIndece)

        elif(option=="Leptons"):            
            l1_from_Z   = self.__TruthLeptons["l1_from_Z"]
            l2_from_Z   = self.__TruthLeptons["l2_from_Z"]
            l_from_t    = self.__TruthLeptons["l_from_t"]
            l_from_tbar = self.__TruthLeptons["l_from_tbar"]

            RecoLeptonVectors = list([item.Vector() for key,item in self.__RecoLeptons.items() if item !=None])
            RecoLeptonVectorsInfos = np.empty(len(RecoLeptonVectors) ,dtype = [("p4", TLorentzVector), ("charge", np.int32), ("flavour", np.int32)])
            RecoLeptonVectorsInfos["p4"]        = RecoLeptonVectors
            RecoLeptonVectorsInfos["charge"]    = [item.Charge() for key, item in self.__RecoLeptons.items() if item != None]
            RecoLeptonVectorsInfos["flavour"]   = [item.Type() for key,item in self.__RecoLeptons.items() if item !=None]
 
            TruthLeptonVectors = [self.__TruthLeptons[key].Vector() for key,value in self.__LeptonIndeceDict.items() if self.__TruthLeptons[key]!=None]
            TruthLeptonVectorsInfos = np.empty(len(TruthLeptonVectors) ,dtype = [("p4", TLorentzVector), ("charge", np.int32), ("flavour", np.int32)])
            TruthLeptonVectorsInfos["p4"]       = [self.__TruthLeptons[key].Vector() for key,value in self.__LeptonIndeceDict.items() if self.__TruthLeptons[key]!=None]
            TruthLeptonVectorsInfos["charge"]   = [self.__TruthLeptons[key].Charge() for key,value in self.__LeptonIndeceDict.items() if self.__TruthLeptons[key]!=None]
            TruthLeptonVectorsInfos["flavour"]  = [self.__TruthLeptons[key].Type() for key,value in self.__LeptonIndeceDict.items() if self.__TruthLeptons[key]!=None]

            RecoLeptonIndece = MatchRecoTruth([value+1 for key,value in self.__LeptonIndeceDict.items() if self.__TruthLeptons[key]!=None], TruthLeptonVectorsInfos, RecoLeptonVectorsInfos, DR_cut = 0.1) # Here the matching for leptons happens
            
            InvMassApproach = TryInvMassApproach(RecoLeptonVectorsInfos, RecoLeptonIndece)

            if(0 in RecoLeptonIndece and 1 in RecoLeptonIndece):
                self.__Z_matched = True
            elif((0 in RecoLeptonIndece) != (1 in RecoLeptonIndece)):
                self.__Z_half_matched = True

            if(2 in RecoLeptonIndece):
                self.__l_from_t_matched = True
            if(3 in RecoLeptonIndece):
                self.__l_from_tbar_matched = True

            if(nItems > len(RecoLeptonIndece)):
               RecoLeptonIndece.extend([-2] * (nItems - len(RecoLeptonIndece)))
            return InvMassApproach, list(RecoLeptonIndece)
        else:
            ErrorMessage("Unrecognized reconstruction option for truth-reco matching!")

    def is3LReco(self):
        return self.__isLeptonicReco

    def is4LReco(self):
        return self.__isDileptonReco

    def NumberLepCombis(self):
        if self.is3LReco():
            return 3 # 3 lepton combinations possible, if 3 leptons on reco level
        elif self.is4LReco():
            return 6 # 6 lepton combinations possible, if 4 leptons on reco level

    def is3L(self):
        return self.__isLeptonic

    def is4L(self):
        return self.__isDilepton

    def isUndef(self):
        return self.__isUndef

    def isZMatched(self):
        return self.__Z_matched

    def isZHalfMatched(self):
        return self.__Z_half_matched

    def nNonZLeptonsMatched(self):
        return sum(self.__l_from_t_matched, self.__l_from_tbar_matched)

    def nZLeptonsMatched(self):
        if(isZMatched(self)):
            return 2
        elif(isZHalfMatched(self)):
            return 1
        else:
            return 0
        
    def nbMatched(self):
        return sum(self.__b_from_t_matched, self.__b_from_tbar_matched)

    def isHadronicMatched(self):
        return (self.is3L() and ((self.__q1_from_t_matched and self.__q2_from_t_matched and self.__b_from_t_matched) or (self.__q1_from_tbar_matched and self.__q2_from_tbar_matched and self.__b_from_tbar_matched)))

    def isLeptonicMatched(self):
        return (self.is3L() and ((self.__l_from_t_matched and self.__b_from_t_matched) or (self.__l_from_tbar_matched and self.__b_from_tbar_matched)))

    def isFullyMatched(self):
        return self.isHadronicMatched() and self.isLeptonicMatched()
    
    def get_NClasses(self):
        return self.__NClasses
                
    def get_ZIndece(self, Nlep):
        invmassclass, indece = self.MatchWithTruth(Nlep, option="Leptons")
        return [indece.index(i) if i in indece else -1 for i in [0,1]]
        
    def get_ZIndece_1D(self, Nlep):
        Zindece = self.get_ZIndece(Nlep)
        # if there are 3 leptons, the following combinations are sufficient:
        if -1 in Zindece:
            if self.NumberLepCombis() == self.get_NClasses(): 
                return -2
            else:
                if self.is3LReco():
                    return 4
                elif self.is4LReco():
                    return 6
        elif 0 in Zindece and 1 in Zindece:
            return 0
        elif 0 in Zindece and 2 in Zindece:
            return 1
        elif 1 in Zindece and 2 in Zindece:
            return 2
        #if there are 4 leptons, one needs additional combinations:
        elif 0 in Zindece and 3 in Zindece:
            return 3
        elif 1 in Zindece and 3 in Zindece:
            return 4
        elif 2 in Zindece and 3 in Zindece:
            return 5

    def get_InvMassClasses(self, Nlep):
        invmassclass, indece = self.MatchWithTruth(Nlep, option="Leptons")
        if invmassclass == -2:
            if self.get_NClasses() == 3: # 3 classes defined -> "no class" is no separate class, no training one these events
                return -2
            elif self.get_NClasses() == 4: # 4 classes defined -> "no class" is a seperate class, train also on those events
                return 3
            else: ErrorMessage("{} classes defined, but need 4 classes, if DNN should be trained on \"no class events\" or 3, if not".format(self.get_NClasses()))
        else: 
            return invmassclass
