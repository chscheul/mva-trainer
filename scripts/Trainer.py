#!/usr/bin/env python3

import numpy as np

import argparse, itertools,  os, sys
import HelperModules.HelperFunctions as hf
from HelperModules import Settings
from HelperModules.Directories import Directories
from HelperModules.DataHandler import DataHandler
from HelperModules.MessageHandler import ErrorMessage, WelcomeMessage, EnvironmentalCheck
from shutil import copyfile

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-c", "--configfile", help="Config file", required=True)
    args = parser.parse_args()

    WelcomeMessage("Trainer")
    EnvironmentalCheck()

    # Here we create the necessary settings objects and read information from the config file
    cfg_settings = Settings.Settings(args.configfile, option="all")
    nFolds       = cfg_settings.get_General().get_Folds()
    Model        = cfg_settings.get_Model()
    Dirs         = Directories(cfg_settings.get_General().get_Job())

    # Copy the config file to the job directory
    copyfile(args.configfile, hf.ensure_trailing_slash(Dirs.ConfigDir())+os.path.basename(args.configfile).replace(".cfg","_training_step.cfg"))
    cfg_settings.Print()
    
    DH = DataHandler(cfg_settings)
    for Fold in range(nFolds):
        cfg_settings.get_Model().CompileAndTrain(DH.get_TrainInputs(Fold=Fold, returnNonZeroLabels=False, returnNominalOnly=False).values, DH.get_TrainLabels(Fold=Fold, returnNonZeroLabels=False, returnNominalOnly=False).values, DH.get_TrainWeights_sc(Fold=Fold, returnNonZeroLabels=False, returnNominalOnly=False).values, Dirs.ModelDir(), Fold=str(Fold), Variables=None)
