from ROOT import TH1D, TH2D, TCanvas, THStack, TLegend, TGraph, TMultiGraph, TMVA, gPad, gStyle, gROOT, gErrorIgnoreLevel, TPad, TGaxis, kBlack, kRed, kBlue, TLine, kWarning
import copy,json,itertools
import numpy as np
import HelperModules.HelperFunctions as hf
import pandas as pd
import HelperModules.AtlasStyle

from pickle import load
from array import array
from HelperModules.AtlasLabel import ATLASLabel, CustomLabel, DrawLabels
from HelperModules.DataHandler import DataHandler
from HelperModules.Histograms import Histograms
from HelperModules.Metrics import Metrics
from HelperModules.MessageHandler import ErrorMessage, PostProcessorMessage, WarningMessage
from tensorflow.keras.models import load_model
from root_numpy import array2hist
from tensorflow.keras.utils import plot_model
from sklearn.metrics import confusion_matrix, roc_auc_score, roc_curve

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

class ClassificationPlotHandler(DataHandler):
    def __init__(self,Settings):
        super().__init__(Settings)
        self.__ALabel  = self._DataHandler__GeneralSettings.get_ATLASLabel()
        self.__CMLabel = self._DataHandler__GeneralSettings.get_CMLabel()
        self.__MyLabel = self._DataHandler__GeneralSettings.get_CustomLabel()
        self.__Hists   = Histograms()
        self.__Model   = self.get_ModelSettings()

        self.__DEFAULTCLASSLABEL    = "Binary"
        if self.__Model.get_ClassColors() != None:
            self.__ClassColorDict   = dict(zip(np.unique(self.get_DataFrame()[self._DataHandler__LABELNAME]),self.__Model.get_ClassColors()))
        else:
            self.__ClassColorDict = None
        self.__isBinary             = len(self.get_ClassLabels()) == 1 or len(self.get_ClassLabels()) == 0
        if self.__isBinary:
            self.__BinDict = {self.__DEFAULTCLASSLABEL: self.__Model.get_ModelBinning()}
        else:
            self.__BinDict = dict(zip(self.get_ClassLabels(),self.__Model.get_ModelBinning()))
        self.__SCALEFACTOR          = 1.4
        self.__SCALEFACTOR_DATAHIST = 1.7
        self.__RATIOPLOTMAX         = 1.55
        self.__RATIOPLOTMIN         = 0.45
        
        gROOT.SetBatch()

        ###################################################################
        ######################## Model Information ########################
        ###################################################################
        if(self.get_nFolds()!=1):
            Folds = ["_Fold"+str(i) for i in range(self.get_nFolds())]
        else:
            Folds = [""]

        if self.__Model.isClassificationDNN() or self.__Model.isReconstruction():
            self.__ModelNames = [hf.ensure_trailing_slash(hf.ensure_trailing_slash(self.get_ModelDirectory()))+Settings.get_Model().get_Name()+"_"+Settings.get_Model().get_Type()+Fold+".h5" for Fold in Folds]
            self.__HistoryNames = [hf.ensure_trailing_slash(hf.ensure_trailing_slash(self.get_ModelDirectory()))+Settings.get_Model().get_Name()+"_"+Settings.get_Model().get_Type()+"_history"+Fold+".json" for Fold in Folds]
            self.__ModelObjects = [load_model(modelname, compile=False) for modelname in self.__ModelNames]
        if self.__Model.isClassificationBDT():
            self.__ModelNames = [hf.ensure_trailing_slash(hf.ensure_trailing_slash(self.get_ModelDirectory()))+Settings.get_Model().get_Name()+"_"+Settings.get_Model().get_Type()+Fold+".pkl" for Fold in Folds]
            self.__ModelObjects = []
            for modelname in self.__ModelNames:
                with open(modelname, 'rb') as f:
                    self.__ModelObjects.append(load(f))
        self.__Scaler = load(open(hf.ensure_trailing_slash(self.get_ModelDirectory())+"scaler.pkl",'rb'))

        ###################################################################
        ########################### Predictions ###########################
        ###################################################################
        F_tmp = [self.get_DataFrame_sc()[self.get_DataFrame_sc()[self._DataHandler__FOLDIDENTIFIER]%self.get_nFolds()==i]["Index_tmp"].values for i in range(self.get_nFolds())]
        X_tmp = [(self.get_DataFrame_sc()[self.get_DataFrame_sc()[self._DataHandler__FOLDIDENTIFIER]%self.get_nFolds()==i][self.get_Variables()]).values for i in range(self.get_nFolds())]
        if self.__Model.isClassificationDNN() or self.__Model.isReconstruction():
            P_tmp = [model.predict(x) for x,model in zip(X_tmp,self.__ModelObjects)]
        if self.__Model.isClassificationBDT():
            if self.__isBinary == True:
                P_tmp = [model.predict_proba(x)[:,1] for x,model in zip(X_tmp,self.__ModelObjects)] # scikit-learn provides both [1-P,P]. We only want P.
            else:
                P_tmp = [model.predict_proba(x) for x,model in zip(X_tmp,self.__ModelObjects)]
        if self.get_nFolds()!=1:
            P_tmp = np.array(list(itertools.chain.from_iterable(P_tmp)))
            F_tmp = np.array(list(itertools.chain.from_iterable(F_tmp)))
        if self.__isBinary == True:
            P_df = pd.DataFrame(data=F_tmp, columns=["Index_tmp"])
            P_df[self._DataHandler__PREDICTIONNAME] = P_tmp 
        else:
            P_df = pd.DataFrame(data=F_tmp, columns=["Index_tmp"])
            for i, ClassLabel in enumerate(self.get_ClassLabels()):
                P_df[ClassLabel] = P_tmp[:,i]
        self._DataHandler__DataFrame    = pd.merge(self.get_DataFrame(), P_df, how="inner", on=["Index_tmp"])
        self._DataHandler__DataFrame_sc = pd.merge(self.get_DataFrame_sc(), P_df, how="inner", on=["Index_tmp"])
        self._DataHandler__DataFrame    = self._DataHandler__DataFrame.drop(["Index_tmp"],axis=1)
        self._DataHandler__DataFrame_sc = self._DataHandler__DataFrame_sc.drop(["Index_tmp"],axis=1)

    ###########################################################################
    ############################# misc functions ##############################
    ###########################################################################

    def get_Predictions(self, PredictionColumn):
        return self.get_DataFrame()[PredictionColumn]

    def get_ModelObjects(self):
        return self.__ModelObjects

    def get_TrainPredictions(self,ColumnID=0, Fold=0, returnNonZeroLabels=False):
        if self.__Model.isClassificationBDT():
            if self.__isBinary == True:
                ColumnID = 1
            return self.__ModelObjects[Fold].predict_proba(self.get_TrainInputs(Fold=Fold, returnNonZeroLabels=returnNonZeroLabels).values)[:,ColumnID]
        if self.__Model.isClassificationDNN() or self.__Model.isReconstruction():
            return self.__ModelObjects[Fold].predict(self.get_TrainInputs(Fold=Fold, returnNonZeroLabels=returnNonZeroLabels).values)[:,ColumnID]

    def get_TestPredictions(self,ColumnID=0, Fold=0, returnNonZeroLabels=False):
        if self.__Model.isClassificationBDT():
            if self.__isBinary == True:
                ColumnID = 1
            return self.__ModelObjects[Fold].predict_proba(self.get_TestInputs(Fold=Fold, returnNonZeroLabels=returnNonZeroLabels).values)[:,ColumnID]
        if self.__Model.isClassificationDNN() or self.__Model.isReconstruction():
            return self.__ModelObjects[Fold].predict(self.get_TestInputs(Fold=Fold, returnNonZeroLabels=returnNonZeroLabels).values)[:,ColumnID]

    def get_HistoryNames(self):
        return self.__HistoryNames
    
    ###########################################################################
    ############################ Wrapper functions ############################
    ###########################################################################
    def do_Plots(self):
        """
        Wrapper function to produce all available plots in one go.
        """
        if self.__Model.isClassificationDNN() or self.__Model.isReconstruction():
            self.do_ArchitecturePlot()
            self.do_PerformancePlot()
        if len(self.__Model.get_ClassLabels())==3:
            self.do_Stack2D()
        if self.__Model.isReconstruction():
            self.do_WeightedAccuracies()
            self.do_EfficiencyPlot()
        self.do_YieldsPlot()
        self.do_StackPlots()
        self.do_Separations1D()
        self.do_TrainTestPlots()
        self.do_SoverBPlots()
        self.do_CorrelationPlots()
        self.do_BinaryConfusionPlots()
        self.do_ROCCurvePlotsCombined()
        self.do_AUCSummaries()
        self.do_ConfusionMatrix()
        self.do_PermutationImportances()

    def do_PermutationImportances(self):
        """
        Wrapper function to produce all permutation importance plots in one go.
        """
        if self.__isBinary == True:
            self.do_PermutationImportance()
        else:
            for ClassID, ClassLabel in enumerate(self._DataHandler__ClassLabels):
                if ClassID in self.get_Labels().values:
                    self.do_PermutationImportance(PredictionColumn=ClassLabel, ClassLabel=ClassLabel, ClassID=ClassID)
                else:
                    WarningMessage("No event in class {}: No Permutation Importance plot produced.".format(ClassLabel))

    def do_StackPlots(self):
        """
        Wrapper function to produce all stack plots in one go.
        """
        if self.__isBinary == True:
            self.do_StackPlot()
        else:
            for ClassID, ClassLabel in enumerate(self._DataHandler__ClassLabels):
                self.do_StackPlot(PredictionColumn=ClassLabel, ClassLabel=ClassLabel, ClassID=ClassID)

    def do_Separations1D(self):
        """
        Wrapper function to produce all 1D separation plots in one go.
        """
        if self.__isBinary == True:
            self.do_Separation1D()
        else:
            for ClassID, ClassLabel in enumerate(self._DataHandler__ClassLabels):
                if ClassID in self.get_Labels().values:
                    self.do_Separation1D(PredictionColumn=ClassLabel, ClassLabel=ClassLabel, ClassID=ClassID)
                else:
                    WarningMessage("No event in class {}: No 1D separation plot produced.".format(ClassLabel))

    def do_TrainTestPlots(self):
        """
        Wrapper function to produce all TrainTest plots in one go.
        """
        if self.__isBinary == True:
            self.do_TrainTestPlot()
        else:
            for ClassID, ClassLabel in enumerate(self._DataHandler__ClassLabels):
                if ClassID in self.get_Labels().values:
                    self.do_TrainTestPlot(PredictionColumn=ClassLabel, ClassLabel=ClassLabel, ClassID=ClassID)
                else:
                    WarningMessage("No event in class {}: No K-Fold comparison produced.".format(ClassLabel))

    def do_SoverBPlots(self):
        """
        Wrapper function to produce all S over B plots in one go.
        """
        if self.__isBinary == True:
            self.do_SoverBPlot()
        else:
            for ClassID, ClassLabel in enumerate(self._DataHandler__ClassLabels):
                if ClassID in self.get_Labels().values:
                    self.do_SoverBPlot(PredictionColumn=ClassLabel, ClassLabel=ClassLabel, ClassID=ClassID)
                else:
                    WarningMessage("No event in class {}: No signal over background plots produced.".format(ClassLabel))

    def do_CorrelationPlots(self):
        """
        Wrapper function to produce all correlation plots in one go.
        """
        if self.__isBinary == True:
            self.do_CorrelationPlot()
        else:
            for ClassID, ClassLabel in enumerate(self._DataHandler__ClassLabels):
                self.do_CorrelationPlot(PredictionColumn=ClassLabel, ClassLabel=ClassLabel, ClassID=ClassID)

    def do_BinaryConfusionPlots(self):
        """
        Wrapper function to produce all binary confusion plots in one go.
        """
        if self.__isBinary == True:
            self.do_BinaryConfusionPlot()
        else:
            for ClassID, ClassLabel in enumerate(self._DataHandler__ClassLabels):
                self.do_BinaryConfusionPlot(PredictionColumn=ClassLabel, ClassLabel=ClassLabel, ClassID=ClassID)

    def do_ROCCurvePlotsCombined(self):
        """
        Wrapper function to produce all ROC curve plots in one go
        """
        if self.__isBinary == True:
            self.do_ROCCurvePlotCombined()
        else:
            for ClassID, ClassLabel in enumerate(self._DataHandler__ClassLabels):
                self.do_ROCCurvePlotCombined(PredictionColumn=ClassLabel, ClassLabel=ClassLabel, ClassID=ClassID)

    def do_AUCSummaries(self):
        """
        Wrapper function to produce all AUCSummary plots in one go
        """
        if self.__isBinary == True:
            self.do_AUCSummary()
        else:
            for ClassID, ClassLabel in enumerate(self._DataHandler__ClassLabels):
                self.do_AUCSummary(PredictionColumn=ClassLabel, ClassLabel=ClassLabel, ClassID=ClassID)

    def do_WeightedAccuracies(self):
        """
        Wrapper function to produce all Accuracy plots in one go
        """
        if self.__isBinary == True:
            self.do_WeightedAccuracy()
        else:
            for ClassID, ClassLabel in enumerate(self._DataHandler__ClassLabels):
                if ClassID in self.get_Labels().values:
                    self.do_WeightedAccuracy(PredictionColumn=ClassLabel, ClassLabel=ClassLabel, ClassID=ClassID)
                else:
                    WarningMessage("No event in class {}: No accuracy plots produced.".format(ClassLabel))


    ###########################################################################
    ########################### Plotting functions ############################
    ###########################################################################
    def do_Stack2D(self):
        """
        Function to produce a 2D stacked plot for a 3-class multi-class classifier.
        """
        Binning_1   = self.__Model.get_ModelBinning()[0]
        Binning_2   = self.__Model.get_ModelBinning()[1]
        Binning_3   = self.__Model.get_ModelBinning()[2]
        ClassLabels = self.get_ClassLabels()
        
        # Defining histograms filling them and scaling them to unity
        if self.__Model.isClassification():
            hists = hf.DefineAndFill2D(
                Binning_1=[50.0, 0.0, 1.0],
                Binning_2=[50.0, 0.0, 1.0],
                Samples=self._DataHandler__Samples,
                Predictions_1=self._DataHandler__DataFrame.get(ClassLabels[0]).values,
                Predictions_2=self._DataHandler__DataFrame.get(ClassLabels[2]).values,
                N=self.get_Sample_Names().values,
                W=self.get_Weights().values)
        elif self.__Model.isReconstruction():
            hists = hf.DefineAndFill2D(
                Binning_1=[50.0, 0.0, 1.0],
                Binning_2=[50.0, 0.0, 1.0], 
                Samples=self._DataHandler__ClassLabels, 
                Predictions_1=self._DataHandler__DataFrame.get(ClassLabels[0]).values,
                Predictions_2=self._DataHandler__DataFrame.get(ClassLabels[2]).values,
                N=self.get_Labels().values,
                W=self._DataHandler__DataFrame[self._DataHandler__WEIGHTNAME].values,
                SamplesAreClasses = True)

        for key,value in hists.items():
            if(value[1][0]!="Data"):
                if self.__Model.isClassification():
                    PostProcessorMessage("Plotting 2D MC plot for "+key+".")
                    basename = key+"_2DMVA"
                elif self.__Model.isReconstruction():
                    PostProcessorMessage("Plotting 2D MC plot for "+value[1][1]+".") 
                    basename = value[1][1]+"_2DMVA"
                Canvas = TCanvas("c1","c1",800,600)
                value[0].Draw("colz")
                Canvas.SetRightMargin(0.15)
                value[0].GetZaxis().SetTitle("Events")
                value[0].GetXaxis().SetTitle(ClassLabels[0]+" Score")
                value[0].GetYaxis().SetTitle(ClassLabels[2]+" Score")
                value[0].SetMinimum(0)
                DrawLabels(self.__ALabel, self.__CMLabel, self.__MyLabel, Option="MC", align="right_3")
                if self.__Model.isClassification():
                    CustomLabel(0.4, 0.7, key+" Response")
                if self.__Model.isReconstruction():
                    CustomLabel(0.4, 0.7, value[1][1]+" Response")
                Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+".pdf")
                Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+".png")
                Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+".eps")
                del Canvas
            if(value[1][0]=="Data"):
                continue

    def do_StackPlot(self, PredictionColumn=None, ClassLabel=None, ClassID=None):
        """
        Function to plot a Stacked data/MC plot.

        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        ClassLabel       --- Name of the class itself
        """
        if(PredictionColumn==None):
            PredictionColumn=self._DataHandler__PREDICTIONNAME
        if(ClassLabel==None):
            ClassLabel=self.__DEFAULTCLASSLABEL
        basename = self.__Model.get_Name()+"_"+self.__Model.get_Type()
        Blinding = self._DataHandler__GeneralSettings.get_Blinding()

        Canvas = TCanvas("StackPlot","StackPlot",800,600)

        PostProcessorMessage("Plotting Stacked Data/MC plot for "+ClassLabel+" classifier.")
        Binning = self.__BinDict[ClassLabel]
        if self.__Model.isClassification():
            hists = hf.DefineAndFill(Binning,
                                     Samples=self._DataHandler__Samples,
                                     Predictions=self.get_Predictions(PredictionColumn).values,
                                     N=self._DataHandler__DataFrame[self._DataHandler__SAMPLENAME].values,
                                     W=self._DataHandler__DataFrame[self._DataHandler__WEIGHTNAME].values)
            colors = {s.get_Name(returnGroupName=True): s.get_FillColor() for s in self._DataHandler__Samples}
        elif self.__Model.isReconstruction():
            hists = hf.DefineAndFill(Binning,
                                     Samples=self._DataHandler__ClassLabels,
                                     Predictions=self.get_Predictions(PredictionColumn).values,
                                     N=self.get_Labels().values,
                                     W=self._DataHandler__DataFrame[self._DataHandler__WEIGHTNAME].values,
                                     SamplesAreClasses = True)
            if self.__ClassColorDict == None:
                colors = {n:int(color) for n,color in zip(np.unique(self.get_DataFrame()[self._DataHandler__LABELNAME]), range(41,101,int(59/(len(self.get_ClassLabels())))))}
            else:
                colors = self.__ClassColorDict

        Stack = THStack()
        if(len(hists)>=6):
            Legend = TLegend(.50,.75-len(hists)/2.*0.025,.90,.90)
            Legend.SetNColumns(2)
        else:
            Legend = TLegend(.50,.75-len(hists)*0.025,.90,.90)
        Legend.SetBorderSize(0)
        Legend.SetTextFont(42)
        Legend.SetTextSize(0.035)
        Legend.SetFillStyle(0)
        temp_s_Stack = THStack()
        temp_b_Stack = THStack()
        data_hist = None

        for key,value in hists.items():
            if(value[1][0]!="Data"):
                Stack.Add(value[0])
                value[0].SetFillColor(colors[key])
                if(not colors[key]==0):
                    value[0].SetLineWidth(0)
                if self.__Model.isClassification():
                    if self._DataHandler__GeneralSettings.do_Yields()==True:
                        Legend.AddEntry(value[0],"{}  {:.1f}".format(key,value[0].Integral()),"f")
                    else:
                        Legend.AddEntry(value[0],key,"f")
                elif self.__Model.isReconstruction():
                    Legend.AddEntry(value[0], value[1][1], "f")
            if(value[1][0]=="Data"):
                data_hist = value[0]
                if self.__Model.isClassification():
                    if self._DataHandler__GeneralSettings.do_Yields()==True:
                        Legend.AddEntry(value[0],"{}  {:.1f}".format(key,value[0].Integral()),"p")
                    else:
                        Legend.AddEntry(value[0],key,"p")
                elif sel.__Model.isReconstruction():
                    Legend.AddEntry(value[0], value[1][1], "p")
        if(data_hist!=None): # In case we do have a data hist we create a ratio plot
            pad1 = TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
            pad1.SetBottomMargin(0)  # joins upper and lower plot
            pad1.Draw()
            # Lower ratio plot is pad2
            Canvas.cd()  # returns to main canvas before defining pad2
            pad2 = TPad("pad2", "pad2", 0, 0.05, 1, 0.3)
            pad2.SetTopMargin(0)  # joins upper and lower plot
            pad2.SetBottomMargin(0.3)
            pad2.Draw()
            d_hist = data_hist.Clone("d_hist")
            MC_hist = Stack.GetStack().Last().Clone("MC_hist")
            ratio = hf.createRatio(d_hist, MC_hist)
            ratio.SetMaximum(self.__RATIOPLOTMAX)
            ratio.SetMinimum(self.__RATIOPLOTMIN)

            # Define blue unc. band and draw it on top of Stack and create legend entry for it
            UpperPad_errband = Stack.GetStack().Last().Clone("UpperPad_errband")
            UpperPad_errband.SetFillColor(kBlue)
            UpperPad_errband.SetFillStyle(3018)
            UpperPad_errband.SetMarkerSize(0)
            Legend.AddEntry(UpperPad_errband,"Uncertainty","f")
            # Define blue unc. band for the lower pad
            LowerPad_errband = Stack.GetStack().Last().Clone("LowerPad_errband")
            for i in range(LowerPad_errband.GetNbinsX()+1):
                if(LowerPad_errband.GetBinContent(i)==0):
                    LowerPad_errband.SetBinError(i,0)
                else:
                    LowerPad_errband.SetBinError(i,LowerPad_errband.GetBinError(i)/LowerPad_errband.GetBinContent(i))
                    LowerPad_errband.SetBinContent(i,1)
            LowerPad_errband.SetFillColor(kBlue)
            LowerPad_errband.SetFillStyle(3018)
            LowerPad_errband.SetMarkerSize(0)

            # Apply blinding
            if(Blinding!=1):
                x_low = data_hist.GetXaxis().FindBin(Blinding)
                for i in range(x_low,LowerPad_errband.GetNbinsX()+1):
                    ratio.SetBinContent(i,-9999)
                    data_hist.SetBinContent(i,-9999)

            # draw everything in first pad
            pad1.cd()
            Stack.Draw("hist")
            UpperPad_errband.Draw("e2 SAME")
            data_hist.Draw("ep SAME")
            # to avoid clipping the bottom zero, redraw a small axis
            axis = Stack.GetYaxis();
            axis.ChangeLabel(1, -1, -1, -1, -1, -1, " ")
            
            # draw everything in second pad
            pad2.cd()
            ratio.Draw("ep")
            ratio.GetXaxis().SetTitle(ClassLabel+" Classifier")
            LowerPad_errband.Draw("e2 SAME")
            
            # switch back to first pad so legend is drawn correctly
            pad1.cd()
        else:
            Stack.Draw("hist")
            Stack.GetXaxis().SetTitle(ClassLabel+" Classifier")
        Stack.GetYaxis().SetTitle("Events")
        # Let's make sure we have enough space for the labels
        Stack.SetMaximum(Stack.GetMaximum()*self.__SCALEFACTOR)
        Legend.Draw()
        DrawLabels(self.__ALabel, self.__CMLabel, self.__MyLabel, Option="data")
        
        # Now we export our plots
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_"+ClassLabel+".pdf")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_"+ClassLabel+".png")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_"+ClassLabel+".eps")
        
        # Clean up
        for key, hist in hists.items():
            gROOT.FindObject("hist"+ str(key)).Delete()
        Canvas.Close()
        del Canvas

    def do_Separation1D(self, PredictionColumn=None, ClassLabel=None, ClassID=None):
        """
        Function to generate a one dimensional separation plot.

        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        ClassLabel       --- Name of the class itself
        ClassID          --- Label associated to the requested class
        """
        if(PredictionColumn==None):
            PredictionColumn=self._DataHandler__PREDICTIONNAME
        if(ClassLabel==None):
            ClassLabel=self.__DEFAULTCLASSLABEL
        basename = self.__Model.get_Name()+"_"+self.__Model.get_Type()

        PostProcessorMessage("Plotting 1D separation plot for "+ClassLabel+" classifier.")
        Binning = self.__BinDict[ClassLabel]
        if self.__Model.isClassification():
            hists = hf.DefineAndFill(Binning,
                                     Samples=self._DataHandler__Samples,
                                     Predictions=self.get_Predictions(PredictionColumn).values,
                                     N=self._DataHandler__DataFrame[self._DataHandler__SAMPLENAME].values,
                                     W=self._DataHandler__DataFrame[self._DataHandler__WEIGHTNAME].values)
        elif self.__Model.isReconstruction():
            hists = hf.DefineAndFill(Binning,
                                     Samples=self._DataHandler__ClassLabels,
                                     Predictions=self.get_Predictions(PredictionColumn).values,
                                     N=self.get_Labels().values,
                                     W=self._DataHandler__DataFrame[self._DataHandler__WEIGHTNAME].values,
                                     SamplesAreClasses = True)
        Canvas = TCanvas("c1","c1",800,600)
        Stack = THStack()
        Legend = TLegend(.63,.75,.85,.85)
        Legend.SetBorderSize(0)
        Legend.SetTextFont(42)
        Legend.SetTextSize(0.035)
        Legend.SetFillStyle(0)
        temp_s_stack = THStack()
        temp_b_stack = THStack()
        for key,value in hists.items():
            if(self.__isBinary==True):
                if(value[1][0]=="Signal"):
                    temp_s_stack.Add(value[0])
                if(value[1][0]=="Background"):
                    temp_b_stack.Add(value[0])
            else:
                # in case of multi class we have multiple classes and always define c as signal
                if self.__Model.isClassification():
                    if(value[1][1]==ClassID):
                        temp_s_stack.Add(value[0])
                    if(value[1][1]!=ClassID):
                        temp_b_stack.Add(value[0])
                if self.__Model.isReconstruction():
                    if(key==ClassID):
                        temp_s_stack.Add(value[0])
                    if(key!=ClassID):
                        temp_b_stack.Add(value[0])
        s_hist = temp_s_stack.GetStack().Last().Clone("s_hist")
        b_hist = temp_b_stack.GetStack().Last().Clone("b_hist")
        s_hist.SetLineColor(2) # red
        b_hist.SetLineColor(4) # blue
        s_hist.Scale(1./s_hist.Integral())
        b_hist.Scale(1./b_hist.Integral())
        Stack.Add(s_hist)
        Stack.Add(b_hist)
        if self.__Model.isReconstruction()==True:
            Legend.AddEntry(s_hist,"Signal","L")
            Legend.AddEntry(b_hist,"Background","L")
        if self.__Model.isClassification()==True:
            Legend.AddEntry(s_hist,ClassLabel,"L")
            Legend.AddEntry(b_hist,"Background","L")
        Stack.Draw("hist nostack")
        Stack.GetYaxis().SetTitle("Fraction of Events")
        Stack.GetXaxis().SetTitle(ClassLabel+" Classifier")

        # Let's make sure we have enough space for the labels and the legend
        Stack.SetMaximum(Stack.GetMaximum()*self.__SCALEFACTOR)
        Legend.Draw()
        DrawLabels(self.__ALabel, self.__CMLabel, self.__MyLabel, Option="MC")
        CustomLabel(0.2,0.7, "Separation=%.2f%%"%(hf.Separation(s_hist, b_hist)*100)) # We want to get percent

        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Separation_"+ClassLabel+".pdf")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Separation_"+ClassLabel+".png")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Separation_"+ClassLabel+".eps")

        # Clean up
        gROOT.FindObject("s_hist").Delete()
        gROOT.FindObject("b_hist").Delete()
        for key, hist in hists.items():
            gROOT.FindObject("hist"+ str(key)).Delete()
        Canvas.Close()
        del Canvas

    def do_SoverBPlot(self, PredictionColumn=None, ClassLabel=None, ClassID=None):
        """
        Function to generate S/B and a S/sqrt(B) plot.

        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        ClassLabel       --- Name of the class itself
        ClassID          --- Label associated to the requested class
        """
        if(PredictionColumn==None):
            PredictionColumn=self._DataHandler__PREDICTIONNAME
        if(ClassLabel==None):
            ClassLabel=self.__DEFAULTCLASSLABEL
        basename = self.__Model.get_Name()+"_"+self.__Model.get_Type()
        PostProcessorMessage("Plotting signal over background plot.")

        basename = self.__Model.get_Name()+"_"+self.__Model.get_Type()
        ClassLabels  = self.get_ClassLabels()
        Binning = self.__BinDict[ClassLabel]
        if self.__Model.isClassification():
            hists = hf.DefineAndFill(Binning,
                                     Samples=self._DataHandler__Samples,
                                     Predictions=self.get_Predictions(PredictionColumn).values,
                                     N=self._DataHandler__DataFrame[self._DataHandler__SAMPLENAME].values,
                                     W=self._DataHandler__DataFrame[self._DataHandler__WEIGHTNAME].values)
        elif self.__Model.isReconstruction():
            hists = hf.DefineAndFill(Binning,
                                     Samples=self._DataHandler__ClassLabels,
                                     Predictions=self.get_Predictions(PredictionColumn).values,
                                     N=self.get_Labels().values,
                                     W=self._DataHandler__DataFrame[self._DataHandler__WEIGHTNAME].values,
                                     SamplesAreClasses = True)
        Canvas = TCanvas("c1","c1",800,600)
        temp_s_stack = THStack()
        temp_b_stack = THStack()
        for key,value in hists.items():
            if(ClassID==None):
                if(value[1][0]=="Signal"):
                    temp_s_stack.Add(value[0])
                if(value[1][0]=="Background"):
                    temp_b_stack.Add(value[0])
            else:
                # in case of multi class we have multiple classes and always define c as signal
                if self.__Model.isClassification():
                    if(value[1][1]==ClassID):
                        temp_s_stack.Add(value[0])
                    if(value[1][1]!=ClassID):
                        temp_b_stack.Add(value[0])
                if self.__Model.isReconstruction():
                    if(key==ClassID):
                        temp_s_stack.Add(value[0])
                    if(key!=ClassID):
                        temp_b_stack.Add(value[0])
        s_hist = temp_s_stack.GetStack().Last().Clone("s_hist")
        b_hist = temp_b_stack.GetStack().Last().Clone("b_hist")
        s_over_b_hist = TH1D("","",s_hist.GetXaxis().GetNbins(), s_hist.GetXaxis().GetXmin(), s_hist.GetXaxis().GetXmax())
        s_over_sqrtb_hist = TH1D("","",s_hist.GetXaxis().GetNbins(), s_hist.GetXaxis().GetXmin(), s_hist.GetXaxis().GetXmax())
        s_over_sqrtb_hist.SetLineColor(4) #blue
        s_over_b_hist.SetLineColor(2) #red
        for b in range(1, s_hist.GetXaxis().GetNbins()+1):
            sum_s_bin_cont = 0
            sum_b_bin_cont = 0
            sum_s_bin_cont = sum([s_hist.GetBinContent(j) for j in range(b, s_hist.GetXaxis().GetNbins()+1)])
            sum_b_bin_cont = sum([b_hist.GetBinContent(j) for j in range(b, b_hist.GetXaxis().GetNbins()+1)])
            if sum_s_bin_cont < 0 or sum_b_bin_cont <= 0: sum_s_bin_cont = 0;  WarningMessage("Detected negative bin yields for bin {}. Ratio was set to zero".format(b))
            else:
                s_over_b_hist.SetBinContent(b, sum_s_bin_cont/sum_b_bin_cont)
                s_over_sqrtb_hist.SetBinContent(b, sum_s_bin_cont/np.sqrt(sum_b_bin_cont))
        s_over_b_hist.Draw("hist")
        if(self.__isBinary==False):
            s_over_b_hist.GetXaxis().SetTitle(ClassLabel+" Classifier")
            s_over_sqrtb_hist.GetXaxis().SetTitle(ClassLabel+" Classifier")
        else:
            s_over_b_hist.GetXaxis().SetTitle("DNN Output")
            s_over_sqrtb_hist.GetXaxis().SetTitle("DNN Output")
        s_over_b_hist.GetYaxis().SetTitle("S/B")
        s_over_sqrtb_hist.GetYaxis().SetTitle("S/#sqrt{B}")

        s_over_b_hist.SetMaximum(s_over_b_hist.GetMaximum()*self.__SCALEFACTOR) 
        DrawLabels(self.__ALabel, self.__CMLabel, self.__MyLabel, Option="MC")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+ClassLabel+"_SoB_Binary.pdf")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+ClassLabel+"_SoB_Binary.png")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+ClassLabel+"_SoB_Binary.eps")
        s_over_sqrtb_hist.Draw()
        s_over_sqrtb_hist.SetMaximum(s_over_sqrtb_hist.GetMaximum()*self.__SCALEFACTOR) 
        DrawLabels(self.__ALabel, self.__CMLabel, self.__MyLabel, Option="MC")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+ClassLabel+"_SosB_Binary.pdf")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+ClassLabel+"_SosB_Binary.png")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+ClassLabel+"_SosB_Binary.eps")
        
        # Doing some cleanup
        gROOT.FindObject("s_hist").Delete()
        gROOT.FindObject("b_hist").Delete()
        for key, hist in hists.items():
            gROOT.FindObject("hist"+ str(key)).Delete()
        Canvas.Close()
        del Canvas

    def do_PerformancePlot(self):
        """
        Plot a typical performance plot using a keras history object.
        The method will check whether the requested metric is indeed available.
        """
        metrics        = self.__Model.get_Metrics()
        metrics.append("loss")
        Metric_Labels = Metrics()
        for i,historyname in enumerate(self.get_HistoryNames()):
            basename = self.__Model.get_Name()+"_"+str(i)
            with open(historyname) as f:
                History = json.load(f)
                for metric in metrics:
                    Canvas = TCanvas("PerformancePlot","PerformancePlot",800,600)
                    Metric_Labels = Metrics()
                    keys = History.keys()
                    mg = TMultiGraph()
                    if(metric in keys):
                        PostProcessorMessage("Plotting training history for metric "+metric+" and fold "+str(i)+".")
                        y = np.array(list(History[metric].values()))
                        x = np.arange(1,len(y)+1,1)
                        gr = TGraph(x.size,x.astype(np.double),y.astype(np.double))
                        gr.SetLineColor(2)
                        gr.SetLineWidth(2)
                        mg.Add(gr)
                        Legend = TLegend(.73,.75,.85,.85)
                        Legend.SetBorderSize(0)
                        Legend.SetTextFont(42)
                        Legend.SetTextSize(0.035)
                        Legend.AddEntry(gr,"Train","L")
                        if("val_"+metric in keys):
                            y = np.array(list(History["val_"+metric].values()))
                            x = np.arange(1,len(y)+1,1)
                            gr_val = TGraph(x.size,x.astype(np.double),y.astype(np.double))
                            gr_val.SetLineColor(4)
                            gr_val.SetLineWidth(2)
                            mg.Add(gr_val)
                            Legend.AddEntry(gr_val,"Validation","L")
                        mg.Draw("A")
                        Legend.Draw("SAME")
                        gPad.SetLogx()
                        mg.SetMaximum(mg.GetHistogram().GetMaximum()*1.3)
                        mg.GetYaxis().SetTitle(Metric_Labels.get_Label(metric))
                        mg.GetXaxis().SetTitle("Epoch")
                        DrawLabels(self.__ALabel, self.__CMLabel, self.__MyLabel, Option="MC")
                        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_"+metric+".pdf")
                        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_"+metric+".png")
                        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_"+metric+".eps")
                    else:
                        WarningMessage("Metric "+metric+" does not exist. No history plot can be created!")
                    Canvas.Close()
                    del Canvas

    def do_PermutationImportance(self, PredictionColumn=None, ClassLabel=None, ClassID=1, nPerms=5):
        """
        Generate a horizontal bar chart wich displays the permutation importance.
        
        
        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        ClassLabel       --- Name of the class itself
        ClassID          --- Label associated to the requested class
        nPerms           --- Number of permutation to be calculated
        """
        if(PredictionColumn==None):
            PredictionColumn=self._DataHandler__PREDICTIONNAME
        if(ClassLabel==None):
            ColumnID = 0
            ClassLabel=self.__DEFAULTCLASSLABEL
        else:
            ColumnID = ClassID
        PostProcessorMessage("Plotting permutation importance for "+ClassLabel+" classifier.")

        Canvas = TCanvas("PermutationImportance","PermutationImportance",800,600)
        auc_list_complete = []

        X = self.get_TestInputs(Fold=0).values
        Y_Pred = self.get_TestPredictions(ColumnID=ColumnID, Fold=0)
        Y = self.get_TestLabels(Fold=0).values
        Y[Y!=ClassID] = -99
        Y[Y==ClassID] = 1
        Y[Y==-99] = 0
        nominal_auc = roc_auc_score(np.array(Y), np.array(Y_Pred)) # Calculate the nominal AUC value as a reference value
        for i in range(len(self.get_Variables())):
            auc_list =[]
            unshuffled = copy.deepcopy(X) # deep copy because shuffle shuffles in place
            for perm in range(nPerms):
                np.random.shuffle(unshuffled[:,i])
                if self.__isBinary==True: # binary case
                    Y_Pred = self.get_ModelObjects()[0].predict(unshuffled)
                else: # multi-class case
                    Y_Pred = self.get_ModelObjects()[0].predict(unshuffled)[:,ClassID]
                auc_list.append((nominal_auc-roc_auc_score(Y, Y_Pred))/nominal_auc)
            auc_list_complete.append(auc_list)
   
        #Calculate mean:
        auc_list = np.mean(auc_list_complete, axis=1)
        # Sort the AUC list and keep track of indece to apply them on Variables later
        indece = sorted(range(len(auc_list)), key=lambda k: auc_list[k], reverse=True)
        auc_sorted = np.array(auc_list)[indece]
        Variables_sorted = np.array(self.get_Variables())[indece]

        # We use a TH1D histogram which we will plot as a hbar chart
        Histogram = TH1D("Histogram","",int(len(auc_sorted)),0,len(auc_sorted))
        Histogram.SetFillColor(4)
        Histogram.SetBarWidth(0.5)
        Histogram.SetBarOffset(0.25) # Since the bar 0.25+0.5+0.25 -> bar is centered with width 0.5
        for bin_ID,mean in enumerate(auc_sorted):
            Histogram.SetBinContent(bin_ID+1,mean)
        Histogram.Draw("hbar")
        DrawLabels(self.__ALabel, self.__CMLabel, self.__MyLabel, Option="MC", align="right_3")
        CustomLabel(0.6,0.7, ClassLabel+" Classifier")
        for bin_ID,var in enumerate(Variables_sorted):
            Histogram.GetXaxis().SetBinLabel(bin_ID+1,self.__Hists.get_Label(var).replace(" [GeV]",""))
        gPad.SetLogx()
        gPad.SetGridx(1)
        Histogram.GetYaxis().SetTitle("(AUC_{nom.}-AUC)/AUC_{nom.}")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+"Permutation_Importance"+ClassLabel+".pdf")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+"Permutation_Importance"+ClassLabel+".png")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+"Permutation_Importance"+ClassLabel+".eps")
        del Canvas

    def do_TrainTestPlot(self, PredictionColumn=None, ClassLabel=None, ClassID=1):
        """
        Generate a plot with superimposed test and train results for signal and background.

        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        ClassLabel       --- Name of the class itself
        ClassID          --- Label associated to the requested class
        """
        if(PredictionColumn==None):
            PredictionColumn=self._DataHandler__PREDICTIONNAME
        if(ClassLabel==None):
            ClassLabel=self.__DEFAULTCLASSLABEL
            ColumnID = 0
        else:
            ColumnID = ClassID
        basename = "TrainTest_"+self.__Model.get_Name()+"_"+self.__Model.get_Type()
        ModelBinning = self.__BinDict[ClassLabel]

        for i in range(self.get_nFolds()):
            PostProcessorMessage("Plotting K-Fold comparison for "+ClassLabel+" classifier, Fold="+str(i))
            Canvas = TCanvas("c1","c1",800,600)
            Train_Signal_Hist = TH1D("Train_Signal_Hist"+str(i),"",int(ModelBinning[0]),ModelBinning[1],ModelBinning[2])
            Test_Signal_Hist = TH1D("Test_Signal_Hist"+str(i),"",int(ModelBinning[0]),ModelBinning[1],ModelBinning[2])
            Train_Background_Hist = TH1D("Train_Background_Hist"+str(i),"",int(ModelBinning[0]),ModelBinning[1],ModelBinning[2])
            Test_Background_Hist = TH1D("Test_Background_Hist"+str(i),"",int(ModelBinning[0]),ModelBinning[1],ModelBinning[2])

            Legend = TLegend(.63,.80-4*0.025,.85,.90)
            Legend.SetBorderSize(0)
            Legend.SetTextFont(42)
            Legend.SetTextSize(0.035)
            Legend.SetFillStyle(0)

            p_train = self.get_TrainPredictions(ColumnID=ColumnID, Fold=i, returnNonZeroLabels=False)
            y_train = self.get_TrainLabels(Fold=i, returnNonZeroLabels=False).values
            w_train = self.get_TrainWeights(Fold=i, returnNonZeroLabels=False).values
            p_test  = self.get_TestPredictions(ColumnID=ColumnID, Fold=i, returnNonZeroLabels=False)
            y_test  = self.get_TestLabels(Fold=i, returnNonZeroLabels=False).values
            w_test  = self.get_TestWeights(Fold=i, returnNonZeroLabels=False).values
        
            hf.FillHist(Train_Signal_Hist, p_train[y_train==ClassID], w_train[y_train==ClassID])
            hf.FillHist(Test_Signal_Hist, p_test[y_test==ClassID], w_test[y_test==ClassID])
            hf.FillHist(Train_Background_Hist, p_train[y_train!=ClassID], w_train[y_train!=ClassID])
            hf.FillHist(Test_Background_Hist, p_test[y_test!=ClassID], w_test[y_test!=ClassID])
            if self.__isBinary == True:
                Legend.AddEntry(Train_Signal_Hist,"Signal (Train)","p")
                Legend.AddEntry(Test_Signal_Hist,"Signal (Test)","f")
            else:
                Legend.AddEntry(Train_Signal_Hist,ClassLabel+" (Train)","p")
                Legend.AddEntry(Test_Signal_Hist,ClassLabel+" (Test)","f")
            Legend.AddEntry(Train_Background_Hist,"Background (Train)","p")
            Legend.AddEntry(Test_Background_Hist,"Background (Test)","f")
            Train_Signal_Hist.Scale(1./Train_Signal_Hist.Integral())
            Test_Signal_Hist.Scale(1./Test_Signal_Hist.Integral())
            Train_Background_Hist.Scale(1./Train_Background_Hist.Integral())
            Test_Background_Hist.Scale(1./Test_Background_Hist.Integral())
            KS_Signal = Train_Signal_Hist.KolmogorovTest(Test_Signal_Hist)
            KS_Background = Train_Background_Hist.KolmogorovTest(Test_Background_Hist)

            maximum = max(Train_Signal_Hist.GetMaximum(),Test_Signal_Hist.GetMaximum(),Train_Background_Hist.GetMaximum(),Test_Background_Hist.GetMaximum())

            Train_Signal_Hist.SetLineColor(2) # red
            Test_Signal_Hist.SetLineColor(2) # red
            Train_Background_Hist.SetLineColor(4) # blue
            Test_Background_Hist.SetLineColor(4) # blue

            Test_Signal_Hist.SetMarkerSize(0)
            Test_Background_Hist.SetMarkerSize(0)
            Train_Signal_Hist.SetMarkerColor(2)
            Train_Background_Hist.SetMarkerColor(4)

            pad1 = TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
            pad1.SetBottomMargin(0)  # joins upper and lower plot
            pad1.Draw()
            # Lower ratio plot is pad2
            Canvas.cd()  # returns to main canvas before defining pad2
            pad2 = TPad("pad2", "pad2", 0, 0.05, 1, 0.3)
            pad2.SetTopMargin(0)  # joins upper and lower plot
            pad2.SetBottomMargin(0.3)
            pad2.Draw()
            sig_ratio = hf.createRatio(Train_Signal_Hist, Test_Signal_Hist)
            bkg_ratio = hf.createRatio(Train_Background_Hist, Test_Background_Hist)
            sig_errband = sig_ratio.Clone("sig_errband")
            bkg_errband = bkg_ratio.Clone("bkg_errband")
            maxerr = 0.55
            for binID in range(sig_errband.GetNbinsX()+1):
                if(sig_errband.GetBinContent(binID)==0):
                    sig_errband.SetBinContent(binID,1)
                    sig_errband.SetBinError(binID,maxerr) # completely fill the ratio plot
                else:
                    sig_errband.SetBinError(binID,sig_errband.GetBinError(binID)/sig_errband.GetBinContent(binID))
                if(bkg_errband.GetBinContent(binID)==0):
                    bkg_errband.SetBinContent(binID,1)
                    bkg_errband.SetBinError(binID,maxerr) # completely fill the ratio plot
                else:
                    bkg_errband.SetBinError(binID,bkg_errband.GetBinError(binID)/bkg_errband.GetBinContent(binID))
            sig_errband.SetFillColor(kRed)
            sig_errband.SetFillStyle(3004)
            sig_errband.SetMarkerSize(0)
            bkg_errband.SetFillColor(kBlue)
            bkg_errband.SetFillStyle(3005)
            bkg_errband.SetMarkerSize(0)
            
            sig_ratio.SetMaximum(1+maxerr)
            sig_ratio.SetMinimum(1-maxerr)
            bkg_ratio.SetMaximum(1+maxerr)
            bkg_ratio.SetMinimum(1-maxerr)
            sig_ratio.SetMarkerSize(0)
            bkg_ratio.SetMarkerSize(0)
            # draw everything in first pad
            pad1.cd()
            Test_Signal_Hist.Draw("hist e")
            Test_Signal_Hist.GetYaxis().SetTitle("Fraction of Events")
            Test_Signal_Hist.GetXaxis().SetTitle("DNN Output")
            Test_Signal_Hist.SetMaximum(maximum*1.5)
            Test_Background_Hist.Draw("hist e SAME")
            Train_Signal_Hist.Draw("e1X0 SAME")
            Train_Background_Hist.Draw("e1X0 SAME")
            if(self.__ALabel.lower()!="none"):
                ATLASLabel(0.175,0.85, self.__ALabel)
            CustomLabel(0.175,0.8, self.__CMLabel)
            CustomLabel(0.175,0.75, "KS Test: Sig.(Bkg.) P=%.3f (%.3f)"%(KS_Signal, KS_Background))
            if(self.__MyLabel!=""):
                CustomLabel(0.2,0.70, self.__MyLabel)
            Legend.Draw("SAME")

            # to avoid clipping the bottom zero, redraw a small axis
            axis = Test_Signal_Hist.GetYaxis();
            axis.ChangeLabel(1, -1, -1, -1, -1, -1, " ")

            # draw everything in second pad
            pad2.cd()
            sig_ratio.Draw("hist")
            bkg_ratio.Draw("hist same")
            sig_errband.Draw("e2 same")
            bkg_errband.Draw("e2 same")
            line = TLine(0,1,1,1)
            line.SetLineStyle(2) # dashed
            line.Draw("Same")
            sig_ratio.GetXaxis().SetTitle(ClassLabel+" Classifier")
            sig_ratio.GetYaxis().SetTitle("Train/Test")

            # switch back to first pad so legend is drawn correctly
            pad1.cd()
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+""+ClassLabel+"_"+str(i)+".pdf")
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+""+ClassLabel+"_"+str(i)+".png")
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+""+ClassLabel+"_"+str(i)+".eps")
            del Train_Signal_Hist, Test_Signal_Hist, Train_Background_Hist, Test_Background_Hist, sig_ratio, bkg_ratio, sig_errband, bkg_errband
            Canvas.Close()
            del Canvas

    def do_CorrelationPlot(self, PredictionColumn=None, ClassLabel=None, ClassID=1):
        """
        Function to produce a correlation plot between inputs and classifier outputs.

        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        ClassLabel       --- Name of the class itself
        ClassID          --- Label associated to the requested class
        """
        if(PredictionColumn==None):
            PredictionColumn=self._DataHandler__PREDICTIONNAME
        if(ClassLabel==None):
            ClassLabel=self.__DEFAULTCLASSLABEL
        PostProcessorMessage("Plotting correlation plot for "+ClassLabel+" classifier.")
        correlations = [[] for i in range(len(self.get_Variables()))]
        X = self.get(self.get_Variables()).values
        if self.__Model.isClassification():
            N = self._DataHandler__DataFrame[self._DataHandler__SAMPLENAME].values
        if self.__Model.isReconstruction():
            N = self.get_Labels().values
            L = {i:n for i,n in enumerate(self.get_ClassLabels())}
            L[-2] = "no-class"  
        P = self.get(PredictionColumn).values
        gStyle.SetPaintTextFormat(".2f")
        Canvas = TCanvas("c1","c1",800,600)
        for i, var in enumerate(self.get_Variables()):
            for n in np.unique(N):
                if n=="Data":
                    continue
                correlations[i].append(np.corrcoef(X[N==n,i],P[N==n])[0][1])
        hist = TH2D("correlationhist","",len(correlations),0,len(correlations),len(correlations[0]),0,len(correlations[0]))
        array2hist(correlations,hist)
        hist.Draw("colz text45")
        Yaxis = hist.GetYaxis()
        Xaxis = hist.GetXaxis()
        if self.__Model.isClassification():
            for i,Sample_Name in enumerate([n for n in np.unique(N) if n!="Data"]):
                Yaxis.SetBinLabel(i+1,Sample_Name)
        if self.__Model.isReconstruction():
            for i,Class_Name in enumerate([n for n in np.unique(N)]):
                Yaxis.SetBinLabel(i+1,L[Class_Name])
        
        for i,var in enumerate(self.get_Variables()):
            Xaxis.SetBinLabel(i+1,self.__Hists.get_Label(var).replace("[GeV]",""))
        Xaxis.LabelsOption("v")
        hist.SetMaximum(1.0)
        hist.SetMinimum(-1.0)
        Canvas.SetRightMargin(0.15)
        Canvas.SetBottomMargin(0.25)
        hist.GetZaxis().SetTitle("Correlation")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+"Correlation_"+ClassLabel+".pdf")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+"Correlation_"+ClassLabel+".png")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+"Correlation_"+ClassLabel+".eps")
        Canvas.Close()
        del Canvas

    def do_BinaryConfusionPlot(self, PredictionColumn=None, ClassLabel=None, ClassID=1):
        """
        Generate a 2D colour plot for all samples to study samples that lead to confusion
        
        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        ClassLabel       --- Name of the class itself
        ClassID          --- LaAccuracyto the requested class
        """
        if(PredictionColumn==None):
            PredictionColumn=self._DataHandler__PREDICTIONNAME
        if(ClassLabel==None):
            ClassLabel=self.__DEFAULTCLASSLABEL
        PostProcessorMessage("Plotting binary confusion plot for "+ClassLabel+" classifier.")
        Binning = self.__BinDict[ClassLabel]
        gStyle.SetPaintTextFormat(".2f")
        Canvas = TCanvas("c1","c1",800,600)
        if self.__Model.isClassification():
            N_dict = {n:i for i,n in enumerate(np.unique(self.get_Sample_Names().values))}
            W_dict = {n:1/np.sum(self.get_Weights().values[self.get_Sample_Names().values==n]) for n in np.unique(self.get_Sample_Names().values)}
            if(len(Binning)==2):
                hist = TH2D("hist","hist",int(Binning[0]),Binning[1],len(N_dict),0,len(N_dict))
            else:
                hist = TH2D("hist","hist",int(Binning[0]),Binning[1],Binning[2],len(N_dict),0,len(N_dict))
            for y_pred,n,w in zip(self.get(PredictionColumn).values,self.get_Sample_Names().values,self.get_Weights().values):
                hist.Fill(y_pred,N_dict[n],w*W_dict[n]*100)
        elif self.__Model.isReconstruction():
            N_dict = {n:i+1 for i,n in enumerate(self.get_ClassLabels())} # keep Bin 0 for "no class"
            N_dict["no-class"] = 0
            L_dict = {n:i for i,n in enumerate(np.unique(self.get_Labels()))}
            W_dict = {n:1/np.sum(self.get_Weights().values[self.get_Labels().values==n]) for n in np.unique(self.get_Labels())}
            if(len(Binning)==2):
                hist = TH2D("hist","hist",int(Binning[0]),Binning[1],len(N_dict),0,len(N_dict))
            else:
                hist = TH2D("hist","hist",int(Binning[0]),Binning[1],Binning[2],len(N_dict),0,len(N_dict))
            for y_pred,n,w in zip(self.get(PredictionColumn).values,self.get_Labels().values,self.get_Weights().values):
                hist.Fill(y_pred,L_dict[n],w*W_dict[n]*100)
        Yaxis = hist.GetYaxis()
        for k,v in N_dict.items():
            Yaxis.SetBinLabel(v+1,k)
        hist.Draw("colz")
        Canvas.SetRightMargin(0.15)
        hist.GetZaxis().SetTitle("Fraction of Events [%]")
        hist.GetXaxis().SetTitle(ClassLabel+" Classifier Output")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+"Confusion_"+ClassLabel+".pdf")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+"Confusion_"+ClassLabel+".png")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+"Confusion_"+ClassLabel+".eps")
        Canvas.Close()
        del Canvas

    def do_ArchitecturePlot(self, left=0.02, right=0.98, bottom=0.02, top=0.98):
        """
        Function to create simple architecture plots of the model.
        
        Keyword arguments:
        left        --- Left border.
        right       --- Right border.
        bottom      --- Bottom border.
        top         --- Top border.
        """
        PostProcessorMessage("Plotting model architecture.")
        plot_model(self.get_ModelObjects()[0], to_file=hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+"Model.pdf")
        plot_model(self.get_ModelObjects()[0], to_file=hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+"Model.png")
        plot_model(self.get_ModelObjects()[0], to_file=hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+"Model.eps")
        if(self.__Model.get_PredesignedModel()==None):
            layer_sizes = [len(self.get_Variables())]
            layer_sizes.extend(self.__Model.get_Nodes())
            layer_sizes.append(self.__Model.get_OutputSize())
            weight_array = []
            for i in range(len(layer_sizes)-1):
                weight_array.append(np.ones((layer_sizes[i],layer_sizes[i+1])))
            fig = plt.figure()
            ax = fig.gca()
            ax.axis('off')
            n_layers = len(layer_sizes)
            v_spacing = (top - bottom)/float(max(layer_sizes))
            h_spacing = (right - left)/float(len(layer_sizes) - 1)
            # Nodes
            for n, layer_size in enumerate(layer_sizes):
                layer_top = v_spacing*(layer_size - 1)/2. + (top + bottom)/2.
                for m in range(layer_size):
                    circle = plt.Circle((n*h_spacing + left, layer_top - m*v_spacing), v_spacing/2.,color='w', ec='k', zorder=4)
                    ax.add_artist(circle)
            # Edges
            for n, (layer_size_a, layer_size_b) in enumerate(zip(layer_sizes[:-1], layer_sizes[1:])):
                layer_top_a = v_spacing*(layer_size_a - 1)/2. + (top + bottom)/2.
                layer_top_b = v_spacing*(layer_size_b - 1)/2. + (top + bottom)/2.
                for m in range(layer_size_a):
                    for o in range(layer_size_b):
                        line = plt.Line2D([n*h_spacing + left, (n + 1)*h_spacing + left],[layer_top_a - m*v_spacing, layer_top_b - o*v_spacing],c="black", linewidth=0.05, zorder=1) 
                        ax.add_artist(line)
            fig.savefig(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+"Model_Graph.pdf")
            fig.savefig(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+"Model_Graph.png")
            fig.savefig(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+"Model_Graph.eps")
            plt.close()

    def do_ROCCurvePlotCombined(self, PredictionColumn=None, ClassLabel=None, ClassID=1):
        """
        Function to draw all ROC curves in one plot.

        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        ClassLabel       --- Name of the class itself
        ClassID          --- Label associated to the requested class
        """
        if(PredictionColumn==None):
            PredictionColumn=self._DataHandler__PREDICTIONNAME
        if(ClassLabel==None):
            ClassLabel=self.__DEFAULTCLASSLABEL
            ColumnID = 0
        else:
            ColumnID = ClassID

        Canvas = TCanvas("ROCCurvePlotCombined","ROCCurvePlotCombined",800,600)
        Legend = TLegend(.63,.75-0.04*self.get_nFolds(),.85,.85) # Legend
        Legend.SetBorderSize(0)
        Legend.SetTextFont(42)
        Legend.SetTextSize(0.035)
        Legend.SetFillStyle(0)
        mg = TMultiGraph()
        TrainGraphs, TestGraphs, TrainAUCs, TestAUCs  = list(), list(), list(), list()
        PostProcessorMessage("Plotting ROC curve for "+ClassLabel+" classifier")
        for i in range(self.get_nFolds()):
            test_mvaTargets  = array('b',(self.get_TestLabels(Fold=i, returnNonZeroLabels=True).values==ClassID).tolist())
            train_mvaTargets = array('b',(self.get_TrainLabels(Fold=i, returnNonZeroLabels=True).values==ClassID).tolist())
            test_mvaValues   = array('f',self.get_TestPredictions(ColumnID=ColumnID,Fold=i, returnNonZeroLabels=True).tolist())
            train_mvaValues  = array('f',self.get_TrainPredictions(ColumnID=ColumnID,Fold=i, returnNonZeroLabels=True).tolist())
            test_mvaWeights  = array('f',self.get_TestWeights(Fold=i, returnNonZeroLabels=True).values.tolist())
            train_mvaWeights = array('f',self.get_TrainWeights(Fold=i, returnNonZeroLabels=True).values.tolist())
            TrainCurve = TMVA.ROCCurve(train_mvaValues, train_mvaTargets, train_mvaWeights)
            TestCurve = TMVA.ROCCurve(test_mvaValues, test_mvaTargets, test_mvaWeights)
            TrainGraphs.append(copy.deepcopy(TrainCurve.GetROCCurve(30)))
            TestGraphs.append(copy.deepcopy(TestCurve.GetROCCurve(30)))
            # Now we calculate the AUC using 100 points as the granularity instead of 30 to be more precise
            TrainAUCs.append(TrainCurve.GetROCIntegral())
            TestAUCs.append(TestCurve.GetROCIntegral())
            Legend.AddEntry(TrainGraphs[i],"Training %d (AUC=%.3f)"%(i+1,TrainAUCs[i]),"L")
            Legend.AddEntry(TestGraphs[i],"Testing %d (AUC=%.3f)"%(i+1,TestAUCs[i]),"L")
            TrainGraphs[i].SetLineColor(kBlue-10+i*2) #blue(ish), we subtract 10 to use the "full" colour wheel 
            TestGraphs[i].SetLineColor(kRed-10+i*2) #red(ish), we subtract 10 to use the "full" colour wheel

            mg.Add(TrainGraphs[i])
            mg.Add(TestGraphs[i])

        # We need some space for the labels
        mg.GetYaxis().SetRangeUser(0.,1.5)
        mg.GetXaxis().SetRangeUser(0.,1.)
        # Setting axis titles
        mg.GetXaxis().SetTitle("Specificity")
        mg.GetYaxis().SetTitle("Sensitivity")
        mg.Draw("AL")
        Legend.Draw("Same")
        DrawLabels(self.__ALabel, self.__CMLabel, self.__MyLabel, Option="MC")
        CustomLabel(0.2,0.70, ClassLabel)
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+"ROCCurve_Combined_"+ClassLabel+".pdf")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+"ROCCurve_Combined_"+ClassLabel+".png")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+"ROCCurve_Combined_"+ClassLabel+".eps")
        Canvas.Close()
        del Canvas

    def do_AUCSummary(self, PredictionColumn=None, ClassLabel=None, ClassID=1):
        """
        Function to draw all ROC curves in one plot.

        Keyword arguments:
        PredictionColumn --- Name of the column holding the prediction
        ClassLabel       --- Name of the class itself
        ClassID          --- Label associated to the requested class
        """
        if(PredictionColumn==None):
            PredictionColumn=self._DataHandler__PREDICTIONNAME
        if(ClassLabel==None):
            ClassLabel=self.__DEFAULTCLASSLABEL
        basename = self.__Model.get_Name()+"_"+self.__Model.get_Type()

        Canvas = TCanvas("AUCSummary","AUCSummary",800,600)
        gStyle.SetPaintTextFormat(".4f")
    
        # get rid of data
        P = self.get(PredictionColumn).values[self.get_Sample_Types().values!="Data"]
        W = self.get_Weights().values[self.get_Sample_Types().values!="Data"]
        if self.__Model.isClassification():
            N = self.get_Sample_Names().values[self.get_Sample_Types().values!="Data"]
            Y = self.get_Labels().values[self.get_Sample_Types().values!="Data"]
            T = self.get_Sample_Types().values[self.get_Sample_Types().values!="Data"]
        if self.__Model.isReconstruction():
            N = self.get_Labels().values[self.get_Sample_Types().values!="Data"]
            Y = self.get_Labels().values[self.get_Sample_Types().values!="Data"]
        if ClassLabel==None:
            NT_dict = dict(zip(N,T))
            Background = [n for n,t in NT_dict.items() if t=="Background"]
            Y[Y!=1] = 0
            AUC = []
            for bkg in Background:
                mvaValues = array('f', P[(N==bkg) | (T=="Signal")])
                mvaTargets = array('b', Y[(N==bkg) | (T=="Signal")])
                mvaWeights = array('f', W[(N==bkg) | (T=="Signal")])
                AUC.append(TMVA.ROCCurve(mvaValues, mvaTargets, mvaWeights).GetROCIntegral())
            PostProcessorMessage("Plotting AUC summary.")
        else:
            if self.__Model.isClassification():
                NY_dict = dict(zip(N,Y))
                NT_dict = dict(zip(N,T))
                Background = [n for n,y in NY_dict.items() if y!=ClassID and NT_dict[n]!="Data"]
            elif self.__Model.isReconstruction():
                Name_dict = {Class:ClassLabel for Class, ClassLabel in enumerate(self.__Model.get_ClassLabels())}
                Name_dict[-2] = "no-class"
                Background = np.unique([y for y in Y if y!=ClassID])
            Y[Y!=ClassID] = -99
            Y[Y==ClassID] = 1
            Y[Y==-99] = 0
            AUC = []
            for bkg in Background:
                mvaValues = array('f', P[(N==bkg) | (Y==1)])
                mvaTargets = array('b', Y[(N==bkg) | (Y==1)])
                mvaWeights = array('f', W[(N==bkg) | (Y==1)])
                AUC.append(TMVA.ROCCurve(mvaValues, mvaTargets, mvaWeights).GetROCIntegral())
            PostProcessorMessage("Plotting AUC summary for "+ClassLabel+" classifier.")
        # Sort the AUC list and keep track of indece
        indece = sorted(range(len(AUC)), key=lambda k: AUC[k], reverse=True)
        AUC_sorted = np.array(AUC)[indece]
        Background_sorted = np.array(Background)[indece]
        
        # We use a TH1D histogram which we will plot as a hbar chart
        Histogram = TH1D("Histogram","",int(len(AUC_sorted)),0,len(AUC_sorted))
        Histogram.SetFillColor(4)
        Histogram.SetBarWidth(0.5)
        Histogram.SetBarOffset(0.25) # Since the bar 0.25+0.5+0.25 -> bar is centered with width 0.5
        for bin_ID,mean in enumerate(AUC_sorted):
            Histogram.SetBinContent(bin_ID+1,mean)
        Histogram.Draw("hist Text")
        DrawLabels(self.__ALabel, self.__CMLabel, self.__MyLabel, Option="MC", align="left")
        for bin_ID,var in enumerate(Background_sorted):
            if self.__Model.isClassification():
                Histogram.GetXaxis().SetBinLabel(bin_ID+1,str(Background_sorted[bin_ID]))
            if self.__Model.isReconstruction():
                Histogram.GetXaxis().SetBinLabel(bin_ID+1,Name_dict[Background_sorted[bin_ID]])
        Histogram.GetYaxis().SetTitle("AUC")
        Histogram.GetXaxis().SetTitle("Background")
        Histogram.SetMaximum(1.3)
        Histogram.SetMinimum(0.5)
        CustomLabel(0.2,0.7, ClassLabel+" Classifier") # To clarify the classifier in multi-class cases
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_AUCSummary_"+ClassLabel+".pdf")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_AUCSummary_"+ClassLabel+".png")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_AUCSummary_"+ClassLabel+".eps")
        del Canvas
    
    def do_WeightedAccuracy(self, PredictionColumn = None, ClassLabel = None, ClassID = None):
        if(PredictionColumn==None):
            PredictionColumn=self._DataHandler__PREDICTIONNAME
        if(ClassLabel==None):
            ClassLabel=self.__DEFAULTCLASSLABEL
        Categories = self.get_Sample_Names()
        Pred = self.get_Predictions(self.get_ClassLabels()).values
        Labels = self.get_Labels().values
        Weights = self.get_Weights().values
        basename = self.__Model.get_Name()+"_"+self.__Model.get_Type()
        PostProcessorMessage("Plotting Accuracy plot for "+ClassLabel+" classifier.")
        accuracy = {}
        precision = {}
        recall = {}
        for Category in np.unique(Categories):
            Y = Pred[Categories == Category]
            N = Labels[Categories == Category]
            W = Weights[Categories == Category]
            TP = FP = FN = TN = noclass = 0
            for y,n,w in zip(Y,N,W):
                class_pred = np.argmax(y)   
                if class_pred == ClassID and n == ClassID:
                    TP += w
                elif class_pred == ClassID and n != ClassID:
                    FP += w
                elif class_pred != ClassID and n == ClassID:
                    FN += w
                elif class_pred != ClassID and n != ClassID and n != -2:
                    TN += w
                elif n == -2:
                    noclass += w
            accuracy[Category] = (TP + TN)/(noclass + TP + TN + FP + FN)
            if TP != 0 or FP != 0:
                precision[Category] = TP/(TP + FP)
            else: precision[Category] = 0
            if TP != 0 or FN != 0:
                recall[Category] = TP/(TP + FN)
            else: recall[Category] = 0
        Hist_Acc = TH1D("HistAcc" + ClassLabel, "", len(accuracy), 0, len(accuracy))           
        Hist_Prec = TH1D("HistPrec" + ClassLabel, "", len(accuracy), 0, len(accuracy))           
        Hist_Rec = TH1D("HistRec" + ClassLabel, "", len(accuracy), 0, len(accuracy))           
        Hist_Acc.SetFillColor(4) #blue
        Hist_Prec.SetFillColor(2) #red 
        Hist_Rec.SetFillColor(3) #green
        Canvas = TCanvas("Accuracy","Accuracy",800,600)
        gStyle.SetPaintTextFormat(".4f")
        for binID, Category in zip(range(1,len(accuracy)+1),accuracy.keys()):
            Hist_Acc.SetBinContent(binID, accuracy[Category])
            Hist_Prec.SetBinContent(binID, precision[Category])
            Hist_Rec.SetBinContent(binID, recall[Category])
        Stack = THStack()
        Stack.Add(Hist_Acc)
        Stack.Add(Hist_Prec)
        Stack.Add(Hist_Rec)
        Stack.Draw("NOSTACKB TEXT")
        for binID, Category in zip(range(1,Hist_Acc.GetNbinsX()+1),accuracy.keys()):
            Stack.GetXaxis().SetBinLabel(binID, Category)
        Stack.SetMaximum(1.5)
        Stack.SetMinimum(0)
        Stack.GetXaxis().SetTitle("Sample")
        Stack.GetYaxis().SetTitle("Accuracy/Precision/Recall")
        CustomLabel(0.2,0.8, ClassLabel + " Classifier")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Accuracy_"+ClassLabel+".pdf")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Accuracy_"+ClassLabel+".png")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Accuracy_"+ClassLabel+".eps")
        del Canvas
        del Hist_Acc
        del Hist_Prec
        del Hist_Rec

    def do_YieldsPlot(self):
        W = self.get_Weights().values
        N_evts = np.ones(len(W)) 
        basename = self.__Model.get_Name()+"_"+self.__Model.get_Type()
        PostProcessorMessage("Plotting class distribution plot.")
        Canvas = TCanvas("ClassDistr", "ClassDistr", 800, 600)
        if self.__Model.isReconstruction():
            Y = self.get_Labels().values
            N = self.get_ClassLabels()
            Yields_Classes = {}
            N_evts_Classes = {}
            for y,n in enumerate(N):
                W_Class = W[Y==y]
                N_evts_Class = N_evts[Y==y]
                Yields_Classes[n] = sum(W_Class)
                N_evts_Classes[n] = sum(N_evts_Class)
            Hist_ClassTotalYields = TH1D("Hist_Total_class", "", len(Yields_Classes), 0, len(Yields_Classes))
            Hist_ClassTotalYields_unw = TH1D("Hist_Total_class_unw", "", len(Yields_Classes), 0, len(Yields_Classes))
            Hist_ClassRelYields = TH1D("Hist_Rel_class", "", len(Yields_Classes), 0, len(Yields_Classes))
            hf.FillYieldHist(Hist_ClassTotalYields, Yields_Classes)
            hf.FillYieldHist(Hist_ClassRelYields, Yields_Classes, total_number_events = sum(W[Y!=-2]))
            hf.FillYieldHist(Hist_ClassTotalYields_unw, N_evts_Classes)
            Hist_ClassTotalYields.GetXaxis().SetTitle("Classes")
            Hist_ClassRelYields.GetXaxis().SetTitle("Classes")
            Hist_ClassTotalYields_unw.GetXaxis().SetTitle("Classes")
            Hist_ClassTotalYields_unw.GetYaxis().SetTitleOffset(1.6) 
            Hist_ClassTotalYields.SetMarkerSize(2)
            Hist_ClassTotalYields_unw.SetMarkerSize(2)
            Hist_ClassRelYields.SetMarkerSize(2)
            gStyle.SetPaintTextFormat(".4f")
            Hist_ClassTotalYields.Draw("HIST TEXT")
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Classes_Yields.pdf")
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Classes_Yields.png")
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Classes_Yields.eps")
            Hist_ClassRelYields.Draw("HIST TEXT")
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Classes_Relative_Yields.pdf")
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Classes_Relative_Yields.png")
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Classes_Relative_Yields.eps")
            gStyle.SetPaintTextFormat(".0f")
            Hist_ClassTotalYields_unw.Draw("HIST TEXT")
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Classes_Yields_unw.pdf")
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Classes_Yields_unw.png")
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Classes_Yields_unw.eps")
            del Hist_ClassTotalYields
            del Hist_ClassRelYields
            del Hist_ClassTotalYields_unw
        if len(np.unique(self.get_Sample_Names())) > 1:
            S = self.get_Sample_Names().values
            N = np.unique(self.get_Sample_Names())
            Yields_Samples = {}
            N_evts_Samples = {}
            for n in N:
                W_Sample = W[S==n]
                N_evts_Sample = N_evts[S==n]
                Yields_Samples[n] = sum(W_Sample)
                N_evts_Samples[n] = sum(N_evts_Sample)
            Hist_SampleTotalYields = TH1D("Hist_Total_sample", "", len(Yields_Samples), 0, len(Yields_Samples))
            Hist_SampleTotalYields_unw = TH1D("Hist_Total_sample_unw", "", len(N_evts_Samples), 0, len(N_evts_Samples))
            Hist_SampleRelYields = TH1D("Hist_Rel_sample", "", len(Yields_Samples), 0, len(Yields_Samples))
            hf.FillYieldHist(Hist_SampleTotalYields, Yields_Samples)
            hf.FillYieldHist(Hist_SampleTotalYields_unw, N_evts_Samples)
            hf.FillYieldHist(Hist_SampleRelYields, Yields_Samples, total_number_events = sum(W))
            Hist_SampleTotalYields.GetXaxis().SetTitle("Samples")
            Hist_SampleRelYields.GetXaxis().SetTitle("Samples")
            Hist_SampleTotalYields_unw.GetXaxis().SetTitle("Samples")
            Hist_SampleTotalYields_unw.GetYaxis().SetTitleOffset(1.6) 
            Hist_SampleTotalYields.SetMarkerSize(2)
            Hist_SampleTotalYields_unw.SetMarkerSize(2)
            Hist_SampleRelYields.SetMarkerSize(2)
            gStyle.SetPaintTextFormat(".4f")
            Hist_SampleTotalYields.Draw("HIST TEXT")
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Samples_Yields.pdf")
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Samples_Yields.png")
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Samples_Yields.eps")
            Hist_SampleRelYields.Draw("HIST TEXT")
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Samples_Relative_Yields.pdf")
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Samples_Relative_Yields.png")
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Samples_Relative_Yields.eps")
            gStyle.SetPaintTextFormat(".0f")
            Hist_SampleTotalYields_unw.Draw("HIST TEXT")
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Samples_Yields_unw.pdf")
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Samples_Yields_unw.png")
            Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Samples_Yields_unw.eps")
            del Hist_SampleTotalYields
            del Hist_SampleRelYields
            del Hist_SampleTotalYields_unw
        del Canvas

    def do_EfficiencyPlot(self):
        basename = self.__Model.get_Name()+"_"+self.__Model.get_Type()
        PostProcessorMessage("Plotting efficiency plot")
        efficiency = {}
        if self.__isBinary:
            PredictionColumn = self._DataHandler__PREDICTIONNAME
        else:
            PredictionColumn = self.get_ClassLabels() 
        for Sample in np.unique(self.get_Sample_Names()):
            Y = self.get_Predictions(PredictionColumn).values[self.get_Sample_Names() == Sample]
            N = self.get_Labels().values[self.get_Sample_Names() == Sample]
            W = self.get_Weights().values[self.get_Sample_Names() == Sample]
            correct = incorrect = noclass = 0
            for y,n,w in zip(Y,N,W):
                class_pred = np.argmax(y)
                if class_pred == n: 
                    correct += w
                elif class_pred != n and n != -2:
                    incorrect += w
            efficiency[Sample] = (correct)/(correct + incorrect)
        Hist_Eff = TH1D("HistEff", "", len(efficiency), 0, len(efficiency))           
        Hist_Eff.SetFillColor(4) #blue
        Canvas = TCanvas("Efficiency","Efficiency",800,600)
        gStyle.SetPaintTextFormat(".4f")
        for binID, Sample in zip(range(1,len(efficiency)+1),efficiency.keys()):
            Hist_Eff.SetBinContent(binID, efficiency[Sample])
        Hist_Eff.Draw("HIST TEXT")
        for binID, Sample in zip(range(1,Hist_Eff.GetNbinsX()+1),efficiency.keys()):
            Hist_Eff.GetXaxis().SetBinLabel(binID, Sample)
        Hist_Eff.SetMaximum(1.5)
        Hist_Eff.SetMinimum(0)
        Hist_Eff.SetMarkerSize(2)
        Hist_Eff.GetXaxis().SetTitle("Sample")
        Hist_Eff.GetYaxis().SetTitle("Efficiency")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Efficiencies.pdf")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Efficiencies.png")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Efficiencies.eps")
        del Canvas
        del Hist_Eff
    
    def do_ConfusionMatrix(self):
        basename = self.__Model.get_Name()+"_"+self.__Model.get_Type()
        PostProcessorMessage("Plotting Confusion matrix")
        gStyle.SetPadRightMargin(0.17)
        if self.__isBinary:
            PredictionColumn = self._DataHandler__PREDICTIONNAME 
            Pred = self.get_Labels().values
            Y_pred = [round(pred) for pred in self.get_Predictions(PredictionColumn)[Pred >= 0]]
            Y_true = Pred[Pred >= 0]
            Classes = ["Background", "Signal"]
            W = self.get_Weights()[Pred >= 0]
        else:
            PredictionColumn = self.get_ClassLabels()
            Probs = self.get_Predictions(PredictionColumn).values
            Pred = self.get_Labels().values
            Y_pred = [np.argmax(prob) for prob in Probs[Pred >= 0]]
            Y_true = Pred[Pred >= 0]
            Classes =  self.get_ClassLabels()
            W = self.get_Weights()[Pred >= 0]
        ConfMatrix = confusion_matrix(Y_true, Y_pred, normalize = "true", sample_weight = W)
        ConfMatrix_Hist  = TH2D("ConfMat", "", len(Classes), 0, len(Classes), len(Classes), 0,len(Classes))
        for binIDx in range(len(Classes)):
            ConfMatrix_Hist.GetXaxis().SetBinLabel(binIDx+1, Classes[binIDx])
            ConfMatrix_Hist.GetYaxis().SetBinLabel(binIDx+1, Classes[binIDx]) 
            for binIDy in range(len(Classes)):
                ConfMatrix_Hist.SetBinContent(binIDx+1, binIDy+1, ConfMatrix[binIDy][binIDx])
        ConfMatrix_Hist.GetXaxis().SetTitle("Prediction") 
        ConfMatrix_Hist.GetYaxis().SetTitle("True Label")
        ConfMatrix_Hist.GetZaxis().SetTitle("Fraction of Events")
        ConfMatrix_Hist.SetMarkerSize(2)
        Canvas = TCanvas("ConfMatrix","ConfMatrix",1000,600)
        gStyle.SetPaintTextFormat(".4f")
        ConfMatrix_Hist.Draw("colz text")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Confusion_matrix.pdf")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Confusion_matrix.png")
        Canvas.SaveAs(hf.ensure_trailing_slash(self.get_MVAPlotDirectory())+basename+"_Confusion_matrix.eps")
        del Canvas
        del ConfMatrix_Hist

        
