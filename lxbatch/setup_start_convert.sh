#!/bin/bash
# Select Repo location and config file you would like to use:
export REPO='/eos/user/c/chscheul/repos/mva-trainer'
export REPO_CONFIG='BatchTestConfig_Convert.cfg'
CONTAINER_SOURCE='docker://gitlab-registry.cern.ch/chscheul/mva-trainer:docker-ci-dev-latest'
export SING_IMAGE='mva_trainer_lxbatch_docker-ci-dev-latest.sif'
SUB_FILE='mva_trainer_convert.sub'
export MVA_EXECUTABLE='mva_singularity_convert.sh'
export OUTPUT_PATH='/eos/user/c/chscheul/Konrad_BSc_Data'

# Setting up log, output and error directories for job outputs
echo "Generating log-file and aux directories..."
mkdir -p ${HOME}/mva_trainer_htcondor/output/
mkdir -p ${HOME}/mva_trainer_htcondor/log/
mkdir -p ${HOME}/mva_trainer_htcondor/error/
mkdir -p ${HOME}/mva_trainer_htcondor/scripts/
mkdir -p ${HOME}/mva_trainer_htcondor/aux/

# Copying executable out of eos
cp -f ${REPO}/lxbatch/executables/${MVA_EXECUTABLE} ${HOME}/mva_trainer_htcondor/scripts/

# Creating and exporting Output_path
mkdir -p ${OUTPUT_PATH}
#echo "export OUTPUT_PATH=${OUTPUT_PATH}" > ${HOME}/mva_trainer_htcondor/aux/output_path.aux

# Checking for Singularity image (and downloading/building it if neccessary)
export SINGULARITY_CACHEDIR="/tmp/$(whoami)/singularity" &&
mkdir -p $SINGULARITY_CACHEDIR
mkdir -p ${REPO}/images/
if [ ! -e ${REPO}/images/${SING_IMAGE} ]
then
  echo "Could not find singularity image '${REPO}/images/${SING_IMAGE}'."
  echo "Building it from ${CONTAINER_SOURCE}..."
  singularity pull --name ${REPO}/images/${SING_IMAGE} ${CONTAINER_SOURCE}
else
  echo "Using existing singularity image '${REPO}/images/${SING_IMAGE}'"
fi
#echo "export SING_IMAGE=${SING_IMAGE}" > ${HOME}/mva_trainer_htcondor/aux/sing_image.aux

# Finally, submitting the job
condor_submit ${REPO}/lxbatch/htcondor_submission/${SUB_FILE}
