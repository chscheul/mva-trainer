Setup Directions for lxbatch:

In this folder lxbatch, there are two bash-scripts for setting up all components required for a training run and submitting said run to HTCondor:

- setup_start_convert.sh (ONLY carries out the conversion from ntuples to hdf5)
- setup_start_job.sh (Carries out conversion, training, and evaluation)

In the scripts, the following variables are important:
- REPO: Gives the absolute path to the mva-trainer repo
- REPO_CONFIG: Gives the name of the .cfg file used (the file must be in the config directory of the repo)
- CONTAINER_SOURCE: URL to the docker-container used. Refer to the GitLab page of the repo for available containers
- SING_IMAGE: Name of the singularity image generated in the repo's images directory. This is only done if no previous image of the same name is present in the directory!
- SUB_FILE: HTCondor submission file to be used for the job
- MVA_EXECUTABLE: Shell script for running the job in HTCondor. The script must be placed in the lxbatch/executables directory of the repo
- OUTPUT_PATH: Absolute path to save the job outputs to (refer to main repo docs for what this entails)

Additionally, a folder mva_trainer_htcondor is created in the user's home directory (/afs/cern.ch/user/n/name/) as direct usage of eos-executables in the submission folder is not possible. Logs, stdout, and stderr are also saved in this folder.

The other file which needs adjusting is obviously the config. Additionally, required sample files currently have to be designated in the respective MVA_EXECUTABLE (in the xrdcp-commands for copying the files to the batch-worker node). This will hopefully be remedied soon by reading out the required files from the config.
